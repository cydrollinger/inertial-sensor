**************************************************************************
* Electronic Realization L.L.C.						 *
* Design: 	9 navigational sensors Accelerometer-XYZ, Gyroscopes-XYZ,* 
*			Magnetometer-XYZ				 *
*						ER 9DOF			 *
* Engineer: Cy Drollinger						 *
* Date: December 15th, 2015						 *	
* Email: cy@elec-real.com						 *
* Address: Box 191 Bozeman, MT 59771-0191				 *
* Phone: 406-539-8117							 *
**************************************************************************

*******************************************
*File Arcitecture plus a terse description*
*******************************************

ROOT:
	document support: snippets of code and images to describe this project
	DriveFiles_12_15: files necessary on the USB drive of the project
	eagleup: 3D files of the physical device
	firmware: the firmware driving the project
	HardWareCad: the electronic design files necessary to manufacture and 
		     or change the project
	readme.txt