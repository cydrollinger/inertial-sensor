

#include  <PPS.h>

/*******************************************************************/
/******** Application specific definitions *************************/
/*******************************************************************/
	
//	#define USE_SELF_POWER_SENSE_IO
//    #define tris_self_power     TRISBbits.TRISB4    // Input wire green self power = 0
    #define self_power          1
//
    #define USE_USB_BUS_SENSE_IO
    #define tris_usb_bus_sense  TRISBbits.TRISB9    // Input wire oragne usb bus sense = 1
    #define USB_BUS_SENSE       1 
    //Uncomment this to make the output HEX of this project 
    //to be able to be bootloaded using the HID bootloader
    #define PROGRAMMABLE_WITH_USB_HID_BOOTLOADER	

    //If the application is going to be used with the HID bootloader
    //  then this will provide a function for the application to 
    //  enter the bootloader from the application (optional)
    #if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
        #define EnterBootloader() __asm__("goto 0x400")
    #endif   

	#define CLOCK_FREQ 32000000

    /** SWITCH *********************************************************/
    #define mInitSwitch2()      TRISAbits.TRISA4=INPUT_PIN; 
	#define Highpower_charge()	TRISBbits.TRISB4=OUTPUT_PIN; _LATB4 = on;
    #define sw2                 PORTAbits.RA4
	    
/** CONSTANT definitions ********************************************/

/** Eleviate magic numbers ******************************************/

#define INPUT_PIN 1
#define OUTPUT_PIN 0
#define on 1
#define off 0
#define one 1
#define zero 0
#define all_ones 0xFF
#define yes	1
#define no 	0
#define clear 0
#define enable 1

