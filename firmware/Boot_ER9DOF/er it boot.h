/********************************************************************
 FileName:     	HardwareProfile - PIC24FJ64GB004 PIM.h
 Dependencies:  See INCLUDES section
 Processor:     PIC24FJ64GB004
 Hardware:      HP_custom
 Compiler:      Microchip C30
 Company:       Eectronic Realization L.L.C. Microchip Technology, Inc.

 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the �Company�) for its PIC� Microcontroller is intended and
 supplied to you, the Company�s customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

********************************************************************
 File Description:

 Change History:
  Rev   Date         Description
  1.0   11/19/2004   Initial release
  2.1   02/26/2007   Updated for simplicity and to use common
                     coding style
  2.3   09/15/2008   Broke out each hardware platform into its own
                     "HardwareProfile - xxx.h" file
  2.4b  04/08/2009   Initial support for PIC24FJ64GB004 family
  3.0	04/00/2010   Hardware and application changes for inertial tracking
********************************************************************/


/*******************************************************************/
/******** Application specific definitions *************************/
/*******************************************************************/
	
	#define USE_SELF_POWER_SENSE_IO
    #define tris_self_power     TRISBbits.TRISB4    // Input wire green self power = 0
    #define self_power          1

    #define USE_USB_BUS_SENSE_IO
    #define tris_usb_bus_sense  TRISAbits.TRISA8    // Input wire oragne usb bus sense = 1
    #define USB_BUS_SENSE       1 
    //Uncomment this to make the output HEX of this project 
    //to be able to be bootloaded using the HID bootloader
    #define PROGRAMMABLE_WITH_USB_HID_BOOTLOADER	

    //If the application is going to be used with the HID bootloader
    //  then this will provide a function for the application to 
    //  enter the bootloader from the application (optional)
    #if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
        #define EnterBootloader() __asm__("goto 0x400")
    #endif   

	#define CLOCK_FREQ 32000000

 /** LED ************************************************************/
    #define mInitAllLEDs()      LATC = 0xFCBF; TRISC = 0xFCBF;
    
    #define mLED_1              LATCbits.LATC6
    #define mLED_2              LATCbits.LATC8
    #define mLED_3              LATCbits.LATC9
    //#define mLED_4              LATCbits.LATC9

    #define mGetLED_1()         mLED_1
    #define mGetLED_2()         mLED_2
    #define mGetLED_3()         mLED_3
    //#define mGetLED_4()         mLED_4     
    
    #define mLED_1_On()         mLED_1 = 1;
    #define mLED_2_On()         mLED_2 = 1;
    #define mLED_3_On()         mLED_3 = 1;
    //#define mLED_4_On()         mLED_4 = 1;
    
    #define mLED_1_Off()        mLED_1 = 0;
    #define mLED_2_Off()        mLED_2 = 0;
    #define mLED_3_Off()        mLED_3 = 0;
    //#define mLED_4_Off()        mLED_4 = 0;
    
    #define mLED_1_Toggle()     mLED_1 = !mLED_1;
    #define mLED_2_Toggle()     mLED_2 = !mLED_2;
    #define mLED_3_Toggle()     mLED_3 = !mLED_3;
    //#define mLED_4_Toggle()     mLED_4 = !mLED_4;

	#define mLED_1_gets_sw2()	mLED_1 = sw2;
	#define mLED_2_gets_sw2()	mLED_2 = sw2;
	#define mLED_3_gets_sw2()	mLED_3 = sw2;

 	#define mSetLED_1(in)         mLED_1 = in
    #define mSetLED_2(in)         mLED_2 = in
    #define mSetLED_3(in)         mLED_3 = in
    
	

   /** SWITCH *********************************************************/
    #define mInitSwitch2()      TRISAbits.TRISA4=INPUT_PIN;  _CN1PUE =0;
    #define mInitSwitch3()      TRISAbits.TRISA9= INPUT_PIN;
    #define mInitAllSwitches()  mInitSwitch2();mInitSwitch3();
    #define sw2                 PORTAbits.RA4
    #define sw3                 PORTAbits.RA9
	
   /** I/O pin definitions *******************************************/
    #define INPUT_PIN 1
    #define OUTPUT_PIN 0
    




