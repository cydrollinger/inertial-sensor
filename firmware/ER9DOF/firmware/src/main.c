/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
********************************************************************/

/** INCLUDES *******************************************************/
#include "usb.h"
#include "usb_device_msd.h"
#include "system_config.h"
#include "system.h"
#include "sd_spi.h"
#include "fileio.h"
#include "string.h"
#include "rtcc.h"
#include "io_mapping.h"
#include "leds.h"
#include "buttons.h"
#include "NineDOF.h"

//External functions in app_device_msd.c
void APP_DeviceMSDInitialize(void);
void APP_DeviceMSDTasks(void);

// GetTimestamp will be called by the FILEIO library when it needs a timestamp for a file
void GetTimestamp (FILEIO_TIMESTAMP * timeStamp);
// Helper function to initialize the RTCC from the compiler timestamp
void RTCCInit (void);
void ReInitialize (void);

// The sdCardMediaParameters structure defines user-implemented functions needed by the SD-SPI fileio driver.
// The driver will call these when necessary.  For the SD-SPI driver, the user must provide
// parameters/functions to define which SPI module to use, Set/Clear the chip select pin,
// get the status of the card detect and write protect pins, and configure the CS, CD, and WP
// pins as inputs/outputs (as appropriate).
// For this demo, these functions are implemented in system.c, since the functionality will change
// depending on which demo board/microcontroller you're using.
// This structure must be maintained as long as the user wishes to access the specified drive.
FILEIO_SD_DRIVE_CONFIG sdCardMediaParameter =
{
    1,                                  // Use SPI module 1
    USER_SdSpiSetCs_1,                  // User-specified function to set/clear the Chip Select pin.
    USER_SdSpiGetCd_1,                  // User-specified function to get the status of the Card Detect pin.
    USER_SdSpiGetWp_1,                  // User-specified function to get the status of the Write Protect pin.
    USER_SdSpiConfigurePins_1           // User-specified function to configure the pins' TRIS bits.
};
extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
// The gSDDrive structure allows the user to specify which set of driver functions should be used by the
// FILEIO library to interface to the drive.
// This structure must be maintained as long as the user wishes to access the specified drive.
const FILEIO_DRIVE_CONFIG gSdDrive =
{
    (FILEIO_DRIVER_IOInitialize)FILEIO_SD_IOInitialize,                      // Function to initialize the I/O pins used by the driver.
    (FILEIO_DRIVER_MediaDetect)FILEIO_SD_MediaDetect,                       // Function to detect that the media is inserted.
    (FILEIO_DRIVER_MediaInitialize)FILEIO_SD_MediaInitialize,               // Function to initialize the media.
    (FILEIO_DRIVER_MediaDeinitialize)FILEIO_SD_MediaDeinitialize,           // Function to de-initialize the media.
    (FILEIO_DRIVER_SectorRead)FILEIO_SD_SectorRead,                         // Function to read a sector from the media.
    (FILEIO_DRIVER_SectorWrite)FILEIO_SD_SectorWrite,                       // Function to write a sector to the media.
    (FILEIO_DRIVER_WriteProtectStateGet)FILEIO_SD_WriteProtectStateGet,     // Function to determine if the media is write-protected.
};

// Declare a state machine for our device
typedef enum
{
    DEMO_STATE_NO_MEDIA = 0,
    DEMO_STATE_MEDIA_DETECTED,
    DEMO_STATE_DRIVE_MOUNTED,
    DEMO_STATE_DONE,
    DEMO_STATE_FAILED
} DEMO_STATE;


bool  i2cInterrupt = FALSE,dataReady = FALSE;
uint8_t i2cComReceive[1024], i2cComSend[256], sampleNumber = 0;
union PackByte byte2word,bytesInFifo;

// Buffer for printing data to file
uint8_t print2file[4096];

extern void my_itoa(int, char *);
void FillFileHeader(void);
/********************************************************************
 * Function:        void main(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Main program entry point.
 *
 * Note:            None
 *******************************************************************/
MAIN_RETURN main(void)
{
    
    char FileName[13] = {"""DATA0000.CSV"""}, ascii[6]={48,48,48,48,48,44};
    int i,j=0;
    FILEIO_OBJECT file;
           
    SYSTEM_Initialize();
    
    USBDeviceInit();
    USBDeviceAttach();
    FILEIO_SD_IOInitialize(&sdCardMediaParameters);
    
    while(1)
    {
        
        SYSTEM_Tasks();

        #if defined(USB_POLLING)
            // Interrupt or polling method.  If using polling, must call
            // this function periodically.  This function will take care
            // of processing and responding to SETUP transactions
            // (such as during the enumeration process when you first
            // plug in).  USB hosts require that USB devices should accept
            // and process SETUP packets in a timely fashion.  Therefore,
            // when using polling, this function should be called
            // regularly (such as once every 1.8ms or faster** [see
            // inline code comments in usb_device.c for explanation when
            // "or faster" applies])  In most cases, the USBDeviceTasks()
            // function does not take very long to execute (ex: <100
            // instruction cycles) before it returns.
            USBDeviceTasks();
        #endif
        //USB not attached interrupt should reinitialize the FILEIO for Recording
        //Operator starts a new recording
            
        
        if(!charge)                                 //Putting the charge pin of the LTC3558 out to red led
            RED_on(less_than_apercent);
        else
            BLUE_on(less_than_apercent);
                    
            //**Initialize FileIO**//
            if(!_SESVD && (BUTTON_IsPressed(NINE_DOF_BUTTON) == TRUE))
                {
                while(BUTTON_IsPressed(NINE_DOF_BUTTON));
            FILEIO_SD_IOInitialize(&sdCardMediaParameter);
            if (!FILEIO_Initialize())
            {
                while(1);
            }
            // Initialize the RTCC from some compiler-generated variables
            RTCCInit();
            // Register the GetTimestamp function as the timestamp source for the library.
            FILEIO_RegisterTimestampGet (GetTimestamp);
            //**Initialize FileIO**//
            
                //MEDIA DETECT
                while(!FILEIO_MediaDetect(&gSdDrive , &sdCardMediaParameter) == true)
                    {
                    //LED_On(LED_D3);
                    }
                // Try to mount the drive we've defined in the gSdDrive structure.
                // If mounted successfully, this drive will use the drive Id 'A'
                // Since this is the first drive we're mounting in this application, this
                // drive's root directory will also become the current working directory
                // for our library.

                //MOUNT DISK
                while (!(FILEIO_DriveMount('A', &gSdDrive , &sdCardMediaParameter) == FILEIO_ERROR_NONE))
                    {
                    //LED_On(LED_D5);
                    }

                //FIND LAST CREATED FILE
                while(FILEIO_Open (&file, FileName, FILEIO_OPEN_READ) == FILEIO_RESULT_SUCCESS)
                    {
                        FileName[7]++;                                  //ones

                        if(FileName[7] == ':')
                        {
                            FileName[7] = '0';
                            FileName[6]++;                              //tens
                                if(FileName[6] == ':')
                                {
                                FileName[6] = '0';
                                FileName[5]++;                          //hundreds
                                    if(FileName[5] == ':')
                                    {
                                    FileName[5] = '0';
                                    FileName[4]++;                      //thousands
                                        if(FileName[4] == ':')
                                        {
                                        FileName[7] = '0';              //reset to 0000
                                        FileName[6] = '0';
                                        FileName[5] = '0';
                                        FileName[4] = '0';
                                        }
                                    }
                                }
                        }
                      FILEIO_Close (&file);
                    }

              //CREATE NEW FILE
              while(FILEIO_Open (&file, FileName, FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND |FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE)
                {
                   //LED_On(LED_D5);
                }
              //FLUSH_FIFO();
              //RECORDING until button pressed
              while(!BUTTON_IsPressed(NINE_DOF_BUTTON))
              {
                //read 20 bytes from MPU-9150
                if(dataReady)
                {  
//                    i2cRead(MPU9150_r,2,FIFO_COUNT_H);
//                    bytesInFifo.HighByte = i2cComReceive[0];
//                    bytesInFifo.LowByte  = i2cComReceive[1];
//                    if(bytesInFifo.word > 140)
                        {
                        i2cRead(MPU9150_r,20,data_reg);
                        for(j=0;j<1;j++)                                                           //0-19 = 20 20 samples x 20 datum = 400 bytes
                        {
                            for(i=0;i<10;i++)
                            {
                            byte2word.HighByte  = i2cComReceive[(j*20) + i*2];                       //data read from MPU-9150 as bytes
                            byte2word.LowByte 	= i2cComReceive[(j*20) + i*2+1];                     //combine bytes into int words and
                            my_itoa(byte2word.word,ascii);                                  //translate to 5 bytes of ascii 2^16
                            print2file[i*6+0] 	= ascii[0];                                 //0-65,536 2's
                            print2file[i*6+1] 	= ascii[1];
                            print2file[i*6+2] 	= ascii[2];
                            print2file[i*6+3] 	= ascii[3];
                            print2file[i*6+4] 	= ascii[4];
                            print2file[i*6+5] 	= ascii[5];                                 //comma separated values
                            }
                            print2file[60]        = 10;
                            if(FILEIO_Write (print2file, 1, (61), &file) == (61))
                            //recording
                            GREEN_on(less_than_apercent); 
                        else
                            //error
                            RED_on(less_than_apercent);
                            dataReady = false;
                            sampleNumber = 0;
                        }
                        
                        }//if bytesinFifo
                 }//if dataready
              }//while ButtonPressed
              
              //CLOSE File
              if(BUTTON_IsPressed(NINE_DOF_BUTTON))
                while(BUTTON_IsPressed(NINE_DOF_BUTTON));
              LED_off();
              while(FILEIO_Close (&file) != FILEIO_RESULT_SUCCESS)
              {
              //error
              RED_on(less_than_apercent);
              }
              //MPU9150_sleep();
              //UNMOUNT Disk
              while(FILEIO_DriveUnmount ('A') == FILEIO_RESULT_FAILURE)
              {
                  //error
                  RED_on(less_than_apercent);
              }
              BLUE_on(less_than_apercent);
        }//end of !VBUS
        APP_DeviceMSDTasks();

    }//end while
}//end main


/*******************************************************************
 * Function:        bool USER_USB_CALLBACK_EVENT_HANDLER(
 *                        USB_EVENT event, void *pdata, uint16_t size)
 *
 * PreCondition:    None
 *
 * Input:           USB_EVENT event - the type of event
 *                  void *pdata - pointer to the event data
 *                  uint16_t size - size of the event data
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is called from the USB stack to
 *                  notify a user application that a USB event
 *                  occured.  This callback is in interrupt context
 *                  when the USB_INTERRUPT option is selected.
 *
 * Note:            None
 *******************************************************************/
bool USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, uint16_t size)
{
    switch((int)event)
    {
        case EVENT_TRANSFER:
            //Add application specific callback task or callback function here if desired.
            break;

        case EVENT_SOF:
            break;

        case EVENT_SUSPEND:
            break;

        case EVENT_RESUME:
            break;

        case EVENT_CONFIGURED:
            /* When the device is configured, we can (re)initialize the demo
             * code. */
            APP_DeviceMSDInitialize();
            break;

        case EVENT_SET_DESCRIPTOR:
            break;

        case EVENT_EP0_REQUEST:
            /* We have received a non-standard USB request.  The MSD driver
             * needs to check to see if the request was for it. */
            USBCheckMSDRequest();
            break;

        case EVENT_BUS_ERROR:
            break;

        case EVENT_TRANSFER_TERMINATED:
            //Add application specific callback task or callback function here if desired.
            //The EVENT_TRANSFER_TERMINATED event occurs when the host performs a CLEAR
            //FEATURE (endpoint halt) request on an application endpoint which was
            //previously armed (UOWN was = 1).  Here would be a good place to:
            //1.  Determine which endpoint the transaction that just got terminated was
            //      on, by checking the handle value in the *pdata.
            //2.  Re-arm the endpoint if desired (typically would be the case for OUT
            //      endpoints).

            //Check if the host recently did a clear endpoint halt on the MSD OUT endpoint.
            //In this case, we want to re-arm the MSD OUT endpoint, so we are prepared
            //to receive the next CBW that the host might want to send.
            //Note: If however the STALL was due to a CBW not valid condition,
            //then we are required to have a persistent STALL, where it cannot
            //be cleared (until MSD reset recovery takes place).  See MSD BOT
            //specs v1.0, section 6.6.1.
            if(MSDWasLastCBWValid() == false)
            {
                //Need to re-stall the endpoints, for persistent STALL behavior.
                USBStallEndpoint(MSD_DATA_IN_EP, IN_TO_HOST);
                USBStallEndpoint(MSD_DATA_OUT_EP, OUT_FROM_HOST);
            }
            else
            {
                //Check if the host cleared halt on the bulk out endpoint.  In this
                //case, we should re-arm the endpoint, so we can receive the next CBW.
                if((USB_HANDLE)pdata == USBGetNextHandle(MSD_DATA_OUT_EP, OUT_FROM_HOST))
                {
                    USBMSDOutHandle = USBRxOnePacket(MSD_DATA_OUT_EP, (uint8_t*)&msd_cbw, MSD_OUT_EP_SIZE);
                }
            }
            break;

        default:
            break;
    }
    return true;
}

// Helper function to initialize the RTCC module.
// This function will use the compiler-generated timestamp to initialize the RTCC.
void RTCCInit (void)
{
    BSP_RTCC_DATETIME dateTime;
    uint8_t weekday;
    uint8_t month;
    uint8_t y;
    uint8_t dateTable[] = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};

    dateTime.bcdFormat = true;

    dateTime.second =  (((__TIME__[6]) & 0x0f) << 4) | ((__TIME__[7]) & 0x0f);
    dateTime.minute =  (((__TIME__[3]) & 0x0f) << 4) | ((__TIME__[4]) & 0x0f);
    dateTime.hour = (((__TIME__[0]) & 0x0f) << 4) | ((__TIME__[1]) & 0x0f);
    dateTime.day =  (((__DATE__[4]) & 0x0f) << 4) | ((__DATE__[5]) & 0x0f);
    dateTime.year = (((__DATE__[9]) & 0x0f) << 4) | ((__DATE__[10]) & 0x0f);

    //Set the month
    switch(__DATE__[0])
    {
        case 'J':
            //January, June, or July
            switch(__DATE__[1])
            {
                case 'a':
                    //January
                    month = 0x01;
                    break;
                case 'u':
                    switch(__DATE__[2])
                    {
                        case 'n':
                            //June
                            month = 0x06;
                            break;
                        case 'l':
                            //July
                            month = 0x07;
                            break;
                    }
                    break;
            }
            break;
        case 'F':
            month = 0x02;
            break;
        case 'M':
            //March,May
            switch(__DATE__[2])
            {
                case 'r':
                    //March
                    month = 0x03;
                    break;
                case 'y':
                    //May
                    month = 0x05;
                    break;
            }
            break;
        case 'A':
            //April, August
            switch(__DATE__[1])
            {
                case 'p':
                    //April
                    month = 0x04;
                    break;
                case 'u':
                    //August
                    month = 0x08;
                    break;
            }
            break;
        case 'S':
            month = 0x09;
            break;
        case 'O':
            month = 0x10;
            break;
        case 'N':
            month = 0x11;
            break;
        case 'D':
            month = 0x12;
            break;
    }

    dateTime.month = month;

    // Start with weekday = 6.  This value is valid for this algorithm for this century.
    weekday = 6;
    // y = year
    y = ((dateTime.year >> 4) * 10) + (dateTime.year & 0x0f);
    // Weekday = base day + year + x number of leap days
    weekday += y + (y / 4);
    // If the current year is a leap year but it's not March, subtract 1 from the date
    if (((y % 4) == 0) && (month < 3))
    {
        weekday -= 1;
    }
    // Add an offset based on the current month
    weekday += dateTable[month - 1];
    // Add the current day in the month
    weekday += ((dateTime.day >> 4) * 10) + (dateTime.day & 0x0f);
    weekday = weekday % 7;

    dateTime.weekday = weekday;

    // Initialize the RTCC with the calculated date/time.
    BSP_RTCC_Initialize (&dateTime);
}

void GetTimestamp (FILEIO_TIMESTAMP * timeStamp)
{
    BSP_RTCC_DATETIME dateTime;

    dateTime.bcdFormat = false;

    BSP_RTCC_TimeGet(&dateTime);

    timeStamp->timeMs = 0;
    timeStamp->time.bitfield.hours = dateTime.hour;
    timeStamp->time.bitfield.minutes = dateTime.minute;
    timeStamp->time.bitfield.secondsDiv2 = dateTime.second / 2;

    timeStamp->date.bitfield.day = dateTime.day;
    timeStamp->date.bitfield.month = dateTime.month;
    // Years in the RTCC module go from 2000 to 2099.  Years in the FAT file system go from 1980-2108.
    timeStamp->date.bitfield.year = dateTime.year + 20;;
}

/*******************************************************************************
 End of File
*/

