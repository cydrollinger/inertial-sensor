/********************************************
Company: 	Electronic Realization L.L.C.

Address: 	313 W. Mendenhall #5
			Bozeman, MT 59715

Phone: 		406-586-5502
Email: 		cy@elec-real.com
Author: 	Cy Drollinger
Date: 		12/07/13

*********
HARDWARE 9DOF
*********
Microchip MCU: PIC24FJ64B004
GPIO
	RA4			BTN1
	
	PIN 2 - 5	SD Micro 
SPI	
	RC8			SCK_MEM		max 20 - 50 Mhz			(4DOF was RC6)
	RC7			SDI_MEM									
	RC9			SDO_MEM								(4DOF was RC8)
	RC6			SS_MEM								(4DOF was RC9)
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX

	PIN 19,20,23,24
				MPU-9150
GPIO
	RB3			FSYNC
	RB2			INT
I2C
	RP6			SCL
	RP5			SDA
USB
	RB10		D+
	RB11		D-

	PIN32-33 LTC3558
GPIO
	RA8			CHARGE
	RB4			HPR
LEDS
	RB13		RED		
	RB15		GREEN
	RB14		BLUE

Nordic Semiconductor ANT+ : nRF24AP2
Invensense 9DOF: MPU-9150
Linear Tecnolgy power management: LTC3558 

*************
Description
*************
	INERTIAL SENSOR WITH NINE DEGREES OF FREEDOM (9DOF)
		3 AXIS ACCELEROMETER
		3 AXIS GYROSCOPE
		3 AXIS MAGNOMETER
		INVENSENE'S MPU-9150
		
	USB MASS STORAGE DEVICE WITH SD MICRO CARD

	USB RECHARGEABLE BATTERY with LTC3558

	WIRELESS COMMUNICATION UP TO 20kb/s with nRF24AP2
	
*******************************************/

#ifndef NineDOF_H 
#define NineDOF_H

#define MPU9150_w 0b11010000
#define MPU9150_r 0b11010001

//MPU-9150 registers and bits
#define magData			0x03				//assosiates 6 byte data with the start 0x03 ends at 0x08
#define MAG_CNTL		0x0A
#define SMPLRT_DIV		0x19				//dependent on the dlpf of accell config / by 1k or 8k
		#define oneHundredHz	0x4F
		#define slow			0xFF
		#define	fourHundred		0x09
#define CONFIG			0x1A
		#define	dlpf			0x03
#define GYRO_CONFIG 	0x1B
		#define full_degrees	0x18
		#define twofifty_deg	0x00
		#define test_gyros		0xE0		//self test all gyros and scale at 250deg/s
		#define no_gyro_st		0x00		//no sefl test configured scale at 250deg/s
		#define	st_gyroX		0x80
		#define	st_gyroY		0x40
		#define	st_gyroZ		0x20
#define ACCEL_CONFIG 	0x1C
		#define test_accs		0xFF
		#define no_accel_st		0x10
		#define eightg			0x10
		#define	st_accellX		0x90
		#define	st_accellY		0x50
		#define	st_accellZ		0x30
		#define sixteenG		0x18
#define FIFO_EN			0x23
		#define allSensors		0xF9
		#define no_mag			0xF8
		#define no_mag_temp		0x78
		#define accell			0x08
#define i2c_MST_CTRL	0x24
		#define	wait_400kHz		0x4D
#define i2cSLV0_ADDR  	0x25
		#define call_AK8975A_W  0x0C
#define i2cSLV0_REG		0x26
#define i2cSLV0_CTRL	0x27
		#define enable6Reads	0x86
		#define enableOneWrite  0x81

#define i2cSLV1_ADDR  	0x28
		#define call_AK8975A_R	0x8C
#define i2cSLV1_REG		0x29
#define i2cSLV1_CTRL	0x2A
		#define sw6Reads		0xD6			//the SLAV0 will read 6 bytes swap the byte order little big endian grouping is set
		#define enableOneWrite  0x81
#define I2C_MST_CTRL	0x36
		#define	wait_for_es		0x40
#define INT_PIN_CFG		0x37
		#define latched			0x20			//Interrupt high until INT_STATUS regs is read
		#define latch_anyread	0x30
		#define write_aux		0x02
		#define ClkOutEn		0x01
#define INT_ENABLE		0x38
		#define no_interrupts	0x00
		#define data_rdy		0x01
		#define dryNfifo		0x11
#define INT_STATUS		0x3A
	#define fifo_overflow		intStatus.bit_4	
#define data_reg		0x3B
#define i2c_SLV0_DO		0x63	
#define i2c_SLV1_DO		0x64	
		#define powerDown		0x00
		#define singleRead		0x01
		#define selfTest		0x08	
		#define fuseRead		0x0F
#define I2C_MST_DELAY_CTRL		0x67
		#define delay_es_shadow	0x80
#define SIGNAL_PATH_RESET		0x68
		#define allpaths		0x07
#define USER_CTRL		0x6A
		#define i2cMstEnable	0x20
		#define fifo_en			0x40
		#define fifo_i2c_enb	0x60
		#define secert_inv		0xE0
		#define fifo_rst		0x04
		#define all_rst			0x05			
#define POWER_MGMT_1	0x6B
		#define noSleep			0x00
		#define reset			0x80
		#define clkGyroX		0x01
#define POWER_MGMT_2	0x6C
#define FIFO_COUNT_H	0x72
		#define twoBytes		0x02
#define FIFO_COUNT_L	0x73
#define FIFO_R_W		0x74
#define WHO_AM_I		0x75

//#define start			0x00
//#define oneRead			0x01
#define oneWrite		0x01

//MAGNETOMETER



//MPU9150 Function Prototypes
//void Start_I2C1(int,int,int);
void i2cWrite(int,int,int);
void i2cRead(int,int,int);
void INITIALIZE_I2C(void);
void INITIALIZE_MPU9150(void);
void powerUpMPU9150(void);
void MPU9150_Fifo_rst(void);
void FLUSH_FIFO(void);
void MPU9150_sleep(void);
void DelayMs(unsigned);
#endif
