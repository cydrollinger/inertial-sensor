/********************************************
Company: 	Electronic Realization L.L.C.

Address: 	313 W. Mendenhall #5
			Bozeman, MT 59715

Phone: 		406-586-5502
Email: 		cy@elec-real.com
Author: 	Cy Drollinger
Date: 		12/07/13

*********
HARDWARE 9DOF
*********
Microchip MCU: PIC24FJ64B004
GPIO
	RA4			BTN1
	
	PIN 2 - 5	SD Micro 
SPI	
	RC8			SCK_MEM		max 20 - 50 Mhz			(4DOF was RC6)
	RC7			SDI_MEM									
	RC9			SDO_MEM								(4DOF was RC8)
	RC6			SS_MEM								(4DOF was RC9)
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX

	PIN 19,20,23,24
				MPU-9150
GPIO
	RB3			FSYNC
	RB2			INT
I2C
	RP6			SCL
	RP5			SDA
USB
	RB10		D+
	RB11		D-

	PIN32-33 LTC3558
GPIO
	RA8			CHARGE
	RB4			HPR
LEDS
	RB13		RED		
	RB15		GREEN
	RB14		BLUE

Nordic Semiconductor ANT+ : nRF24AP2
Invensense 9DOF: MPU-9150
Linear Tecnolgy power management: LTC3558 

*************
Description
*************
	INERTIAL SENSOR WITH NINE DEGREES OF FREEDOM (9DOF)
		3 AXIS ACCELEROMETER
		3 AXIS GYROSCOPE
		3 AXIS MAGNOMETER
		INVENSENE'S MPU-9150
		
	USB MASS STORAGE DEVICE WITH SD MICRO CARD

	USB RECHARGEABLE BATTERY with LTC3558

	WIRELESS COMMUNICATION UP TO 20kb/s with nRF24AP2
	
*******************************************/

#include "NineDOF.h"
#include "system.h"
//#include "hardwareProfile.h"
//#include "er_it_io.h"

extern uint8_t i2cComSend[], i2cComReceive[];
extern bool i2cInterrupt;
extern int delay, testCount;
int i2cComReceivePtr = 0, i2cComSendPtr = 0, j = 0;


void i2cWrite(int WriteDevice,int WriteHowMany,int WriteWhere)
{
i2cComSendPtr = 0x00;
_SEN = on;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
I2C1TRN = WriteDevice;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
I2C1TRN = WriteWhere;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
while(i2cComSendPtr != WriteHowMany)
{
I2C1TRN = i2cComSend[i2cComSendPtr];
i2cComSendPtr++;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
}
_PEN = on;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
}


void i2cRead(int ReadDevice, int ReadHowMany, int ReadWhere)
{

i2cComReceivePtr = 0x00;
_SEN = on;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
I2C1TRN = MPU9150_w;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
I2C1TRN = ReadWhere;
while(!i2cInterrupt);
i2cInterrupt = FALSE;

_SEN = on;
while(!i2cInterrupt);
i2cInterrupt = FALSE;

I2C1TRN = ReadDevice; 
while(!i2cInterrupt);
i2cInterrupt = FALSE;
while(i2cComReceivePtr != ReadHowMany)
{
_RCEN = on;
while(!i2cInterrupt);                               //must wait for the _RCEN TO COMPLETE 2-3weeks!!!!Thanks JD
i2cInterrupt = FALSE;

i2cComReceive[i2cComReceivePtr] = I2C1RCV;
i2cComReceivePtr++;


if(i2cComReceivePtr == ReadHowMany)
{
_ACKDT = on;
_ACKEN = on;
}
else
{
_ACKDT = off;
_ACKEN = on;
}
while(!i2cInterrupt);
i2cInterrupt = FALSE;
}
_PEN = on;
while(!i2cInterrupt);
i2cInterrupt = FALSE;
while(!SDA);
}

void INITIALIZE_MPU9150(void)
{
I2C1BRG = 0x23;                                 //I2CxBRG = (Fcy/FSCL(400kHz) - Fcy/10MHz) - 1 pg 179 pic ds
_ACKDT = 1;
_I2CEN = on;

powerUpMPU9150();
}

void powerUpMPU9150(void)
{
i2cComSend[0] = reset;                          //0x80
i2cWrite(MPU9150_w,oneWrite,POWER_MGMT_1);		//0x6B
DelayMs(50);
i2cComSend[0] = clkGyroX;                       //0x01
i2cWrite(MPU9150_w,oneWrite,POWER_MGMT_1);		//0x6b
DelayMs(50);                                    //50ms recommended delay after sleep state disabled 
                                                //stablizing the 8kHz clock based on the Xgyroscope (datasheet pg. 11 30 ms)

i2cComSend[0] = oneHundredHz;                   //0x09
i2cWrite(MPU9150_w,oneWrite,SMPLRT_DIV);		//0x19

i2cComSend[0] = sixteenG;                       //0x18
i2cWrite(MPU9150_w,oneWrite,ACCEL_CONFIG);		//0x1C

i2cComSend[0] = full_degrees;                   //0x18
i2cWrite(MPU9150_w,oneWrite,GYRO_CONFIG);		//0x1B

//Initialize AK8975A 

i2cComSend[0] = call_AK8975A_W;                 //(0x0C)AK8975A i2c address
i2cWrite(MPU9150_w,oneWrite,i2cSLV1_ADDR);		//(0x28)Write into i2c SLAVE 0 ADDRess

i2cComSend[0] = MAG_CNTL;                       //(0x0A)Register within the AK8975A from which to read
i2cWrite(MPU9150_w,oneWrite,i2cSLV1_REG);		//(0x29)write into i2c SLAVE 0 register register

i2cComSend[0] = singleRead;                     //(0x01)# of reads from AK8975A
i2cWrite(MPU9150_w,oneWrite,i2c_SLV1_DO);		//(0x64)i2c SLAVE 0 control This is an automatic read within the MPU9150 of the AK8975A
                                                //AK8975A data is synced and stored with the MPU9150 reads into ext data registers 0x49-0x4E

i2cComSend[0] = enableOneWrite;                 //(0x81)# of reads from AK8975A
i2cWrite(MPU9150_w,oneWrite,i2cSLV1_CTRL);		//(0x2A)i2c SLAVE 0 control This is an automatic read within the MPU9150 of the AK8975A
												//AK8975A data is synced and stored with the MPU9150 reads into ext data registers 0x49-0x4E


//Setup Auxiliary i2c Communication for automatic reads on AK8975A
i2cComSend[0] = call_AK8975A_R;                 //(0x8C)AK8975A i2c address
i2cWrite(MPU9150_w,oneWrite,i2cSLV0_ADDR);		//(0x25)Write into i2c SLAVE 0 ADDRess


i2cComSend[0] = magData;                        //(0x03)Register within the AK8975A from which to read
i2cWrite(MPU9150_w,oneWrite,i2cSLV0_REG);		//(0x26)write into i2c SLAVE 0 register register


i2cComSend[0] = sw6Reads;                       //(0xC6)# of reads from AK8975A
i2cWrite(MPU9150_w,oneWrite,i2cSLV0_CTRL);		//(0x27)i2c SLAVE 0 control This is an automatic read within the MPU9150 of the AK8975A
                                                //AK8975A data is synced and stored with the MPU9150 reads into ext data registers 0x49-0x4E
MPU9150_Fifo_rst();

//i2cComSend[0] = dlpf;                           //0x03
//i2cWrite(MPU9150_w,oneWrite,CONFIG);			//0x1A
i2cComSend[0] = allSensors;                     //0xF9
i2cWrite(MPU9150_w,oneWrite,FIFO_EN);			//0x23	
i2cComSend[0] = delay_es_shadow;                //0x40
i2cWrite(MPU9150_w,oneWrite,I2C_MST_DELAY_CTRL);//0x36

i2cComSend[0] = data_rdy;                       //0x01
i2cWrite(MPU9150_w,oneWrite,INT_ENABLE);		//0x38
}

void MPU9150_Fifo_rst(void)
{
//Apply Fifo Reset
i2cComSend[0] = 0x00;                           //0x00
i2cWrite(MPU9150_w,oneWrite,INT_ENABLE);		//0x38
i2cWrite(MPU9150_w,oneWrite,FIFO_EN);			//0x23
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
i2cComSend[0] = fifo_rst;                       //0x04
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
i2cComSend[0] = fifo_i2c_enb;                   //0x40		
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
DelayMs(50);
}

void FLUSH_FIFO(void)
{
//Apply Fifo Reset
i2cComSend[0] = 0x00;                           //0x00
i2cWrite(MPU9150_w,oneWrite,INT_ENABLE);		//0x38
i2cWrite(MPU9150_w,oneWrite,FIFO_EN);			//0x23
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
i2cComSend[0] = fifo_rst;                       //0x04
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
i2cComSend[0] = fifo_i2c_enb;                   //0x40		
i2cWrite(MPU9150_w,oneWrite,USER_CTRL);			//0x6A
//i2cComSend[0] = dlpf;                           //0x03
//i2cWrite(MPU9150_w,oneWrite,CONFIG);			//0x1A
i2cComSend[0] = allSensors;                     //0xF9
i2cWrite(MPU9150_w,oneWrite,FIFO_EN);			//0x23	
i2cComSend[0] = delay_es_shadow;                //0x40
i2cWrite(MPU9150_w,oneWrite,I2C_MST_DELAY_CTRL);//0x36
i2cComSend[0] = data_rdy;                       //0x01
i2cWrite(MPU9150_w,oneWrite,INT_ENABLE);		//0x38
}

void MPU9150_sleep(void)
{
i2cComSend[0] = reset;
i2cWrite(MPU9150_w,0x01,POWER_MGMT_1);
}

void DelayMs(unsigned t)
{
T2CON = 0x8000;
while(t--)
{
TMR2 = 0;
while(TMR2<16000);
}
}

