/********************************************************************
 FileName:     	HardwareProfile - PIC24FJ64GB004 PIM.h
 Dependencies:  See INCLUDES section
 Processor:     PIC24FJ64GB004
 Hardware:      PIC24FJ64GB004 PIM
 Compiler:      Microchip C30
 Company:       Microchip Technology, Inc.

 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the �Company�) for its PIC� Microcontroller is intended and
 supplied to you, the Company�s customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

********************************************************************
 File Description:

 Change History:
  Rev   Date         Description
  1.0   11/19/2004   Initial release
  2.1   02/26/2007   Updated for simplicity and to use common
                     coding style
  2.3   09/15/2008   Broke out each hardware platform into its own
                     "HardwareProfile - xxx.h" file
  2.4b  04/08/2009   Initial support for PIC24FJ64GB004 family
********************************************************************/
#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include "p24FJ64GB004.h"
#include  "PPS.h"

/*******************************************************************/
/******** USB stack hardware selection options *********************/
/*******************************************************************/
//This section is the set of definitions required by the MCHPFSUSB
//  framework.  These definitions tell the firmware what mode it is
//  running in, and where it can find the results to some information
//  that the stack needs.
//These definitions are required by every application developed with
//  this revision of the MCHPFSUSB framework.  Please review each
//  option carefully and determine which options are desired/required
//  for your application.

	//#define USE_SELF_POWER_SENSE_IO
    //#define tris_self_power     TRISBbits.TRISB4    // Input wire green self power = 0
    #define self_power          1

   //#define USE_USB_BUS_SENSE_IO
    //#define tris_usb_bus_sense  TRISAbits.TRISA8    // Input wire oragne usb bus sense = 1
    #define USB_BUS_SENSE       1 
    //Uncomment this to make the output HEX of this project 
    //to be able to be bootloaded using the HID bootloader
    #define PROGRAMMABLE_WITH_USB_HID_BOOTLOADER	

    //If the application is going to be used with the HID bootloader
    //  then this will provide a function for the application to 
    //  enter the bootloader from the application (optional)
    #if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
        #define EnterBootloader() __asm__("goto 0x400")
    #endif

	/*******************************************************************/
    /******** MDD File System selection options ************************/
    /*******************************************************************/
    #define ERASE_BLOCK_SIZE        1024
    #define WRITE_BLOCK_SIZE        128

	#define mLED_1              _LATB9
    #define mLED_2              _LATB13
    #define mLED_3              _LATC8
    //#define mLED_4            _LATC9

    #define mGetLED_1()         mLED_1
    #define mGetLED_2()         mLED_2
    #define mGetLED_3()         mLED_3
    //#define mGetLED_4()         mLED_4     
    
    #define mLED_1_On()         mLED_1 = 1;
    #define mLED_2_On()         mLED_2 = 1;
    #define mLED_3_On()         mLED_3 = 1;
    //#define mLED_4_On()         mLED_4 = 1;
    
    #define mLED_1_Off()        mLED_1 = 0;
    #define mLED_2_Off()        mLED_2 = 0;
    #define mLED_3_Off()        mLED_3 = 0;
    //#define mLED_4_Off()        mLED_4 = 0;
    
    #define mLED_1_Toggle()     mLED_1 = !mLED_1;
    #define mLED_2_Toggle()     mLED_2 = !mLED_2;
    #define mLED_3_Toggle()     mLED_3 = !mLED_3;
    //#define mLED_4_Toggle()     mLED_4 = !mLED_4;

	#define mSetLED_1(in)         mLED_1 = in
    #define mSetLED_2(in)         mLED_2 = in
    #define mSetLED_3(in)         mLED_3 = in
    //#define mSetLED_4(in)         mLED_4 = in

	#define mLED_1_gets_sw2()	mLED_1 = sw2;
	#define mLED_2_gets_sw2()	mLED_2 = sw2;
	#define mLED_3_gets_sw2()	mLED_3 = sw2;
	

   /** SWITCH *********************************************************/
    #define mInitSwitch2()      _TRISA4 = INPUT_PIN; 
    #define mInitSwitch3()      _TRISA9 = OUTPUT_PIN;
    #define INITIALIZE_Switches()  mInitSwitch2(); mInitSwitch3();
    #define sw2                 _RA4
    #define sw3                 _RA9
	
   
    /*******************************************************************/
    /*******************************************************************/
    /*******************************************************************/
    /******** Application specific definitions *************************/
    /*******************************************************************/
    /*******************************************************************/
    /*******************************************************************/

/** Eleviate magic numbers ******************************************/
		/** General**/
#define INPUT_PIN 1
#define OUTPUT_PIN 0
#define on 		1
#define off 	0
#define zero 	0
#define one  	1
#define two   	0x02					//utilized in main as a case for POWER_DOWN(device)
#define three 	0x03
#define four  	0x04

#define firstNibble  0xF0
#define secondNibble 0x0F00
#define thirdNibble  0x00F0
#define lastNibble   0x0F

#define No_connect zero

		/**INERTIAL DATA**/
#define roffset 0x00
#define xoffset 0x01
#define yoffset 0x02
#define zoffset 0x03

#define samples 0x04					//inertial data consists of r,x,y,z four words
#define ThirtyTwoSamples 0x20			//The M25P16 is structered with 256 bytes per page (pp)
#define FFBytes			0xFF

#define adxl 	0x01
#define gyro 	0x04

/**ANT RF DATA**/
#define ant  	0x02

/**PROCESSOR DATA**/
#define cpu 	0x03

/**IO DATA**/
#define leds 	0x05


/**MEMORY DATA**/
#define mem 			0x06
#define FullPage 		0x0400
#define BufferFull		0x007C
#define firstHalf		0x0000
#define secondHalf		0x0001
#define hold			0x0002

#define all 0x07


/*******************************************************************/
/******** Application specific definitions *************************/
/*******************************************************************/


union PackByte
{
 UINT16 word;
 struct 
 	{
    UINT8 LowByte;
    UINT8 HighByte;
  	};
struct
	{
	unsigned bit_1 :1;
	unsigned bit_2 :1;
	unsigned bit_3 :1;
	unsigned bit_4 :1;
	unsigned bit_5 :1;
	unsigned bit_6 :1;
	unsigned bit_7 :1;
	unsigned bit_8 :1;
	unsigned bit_9 :1;
	unsigned bit_10 :1;
	unsigned bit_11 :1;
	unsigned bit_12 :1;
	unsigned bit_13 :1;
	unsigned bit_14 :1;
	unsigned bit_15 :1;
	unsigned bit_16 :1;
	};
 };

union FourBytes
{
 long int two_words;
 struct 
 	{
    INT8 firstByte;										//lsb
    INT8 secondByte;
	INT8 thirdByte;
	INT8 fourthByte;									//msb
  	};
};


//#define USE_SD_INTERFACE_WITH_SPI
//		/**INERTIAL DATA**/

/*******************************************************************/
    /******** MDD File System selection options ************************/
    /*******************************************************************/
    #define USE_SD_INTERFACE_WITH_SPI
    

    // Sample definitions for 16-bit processors (modify to fit your own project)
    #define SD_CD				PORTBbits.RB0
    #define SD_CD_TRIS			TRISBbits.TRISB0
    
    #define SD_WE				PORTBbits.RB1
    #define SD_WE_TRIS			TRISBbits.TRISB1
    
	#define SD_CS				PORTCbits.RC6
    #define SD_CS_TRIS			TRISCbits.TRISC6
    
	// Registers for the SPI module you want to use
    #define SPICON1				SPI1CON1
    #define SPISTAT				SPI1STAT
    #define SPIBUF				SPI1BUF
    #define SPISTAT_RBF			SPI1STATbits.SPIRBF
    #define SPICON1bits			SPI1CON1bits
    #define SPISTATbits			SPI1STATbits
    #define SPIENABLE           SPISTATbits.SPIEN
	#define SPICLOCK			_DISSCK
	#define SPIIN				_SMP
	#define SPIOUT				_DISSDO

	// Tris pins for SCK/SDI/SDO lines
	#define MEM_SDI			0b00111		//Maps SPI1_SDO onto RP23 (function(A) mapps SDO on SPI MCU perspective)
	#define MEM_SCK 		0b01000		//Maps SPI1_SCK onto RP22 
	#define MEM_SDO 		0b11001		//Map RP24 onto SPI1_SDI
//    #define SPICLOCK			TRISCbits.TRISC6
//    #define SPIIN				TRISCbits.TRISC7
//    #define SPIOUT			    TRISCbits.TRISC8

	//other definitions specific to er
	#define GetSystemClock()	32000000
	#define GetInstructionClock() 32000000
		/**ANT RF**/

/*******************************************************************/
/********      Main function declaration   *************************/
/*******************************************************************/
void UserInit(void);
void ASLEEP (int );
void AWAKE (int device);
void SleepDelay(void);
#endif  //HARDWARE_PROFILE_H


