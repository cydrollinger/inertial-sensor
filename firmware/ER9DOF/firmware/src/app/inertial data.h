/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

 #ifndef INERTIAL_DATA_H 
 #define INERTIAL_DATA_H


/*
** ADXL345 accelerometer library
** encapsulates ADXL345 accelerometer 
** as a sensing device for PIC24 
*/

// initialize access to memory device
// 16-bit integer read and write functions
// NOTE: address must be an even value between 0x0000 and 0x7ffe
// (see page write restrictions on the device datasheet)

void INITIALIZE_ADC(void);
int read_ADC (int );
void GYRO_SHUTDOWN(void);
void GYRO_POWERUP(void);

void INITIALIZE_adxl345(void);
int write_adxl345(int);
int Read_Which_Interrupt(void);
void Read_Inertial_data(char *);
void ADXL_POWER_CTL(int);
void ADXL_Enable_Interrupts(int);
int read_ADC (int );
int TwosComplement(int);

//***********************************************************************//
//********************  PIC24FJ64GB004(ADC)/Gyroscope  ******************//
//***********************************************************************//
#define reset_HPF		_LATA10								//Highpass filter reset gyro 503alh
#define Self_Test		_LATA7								//Self Test
#define	AINPUTS			0xF9FF								//puts AN9(out +) and AN10(out -) into analog inputs sent from main into initialize
#define gyro_diffn 		0x000A								//this is the channel for read_ADC
#define gyro_diffp 		0x0009								//this is the channel for read_ADC

//***********************************************************************//
//********************  ADXL345 ACCELEROMETER  **************************//
//***********************************************************************//
//Defining ADXL345 Configuration

//ADXL345 ID
#define ADXL345_ID		0xE5	//device id of the adxl345 in its 00 register

//COMMNADS
#define READ_BYTE 	0x8000		//Two bit command for reading single byte off of SPI adxl345 DS pg. 9
#define WRITE_BYTE	0x0000		// reamining 6bits carries the register address 

#define READ_MB 	0xC000		//Two bit command for reading mulitple bytes off of SPI adxl345 DS pg. 9
#define WRITE_MB	0x4000		//command ored with register bits gives the data to send to SPI


//REGISTERS
//INFORMATION
		//ACTION
#define	DEVID			0x0000	//ADXL345 Registers DS pg.14
//(8b) (r) Value 0xE5
		//compare
		
#define THRES_TAP		0X1D00
#define TWO_g			0x0020
#define FOUR_g			0x0040
#define SIX_g			0x0060
#define EIGHT_g			0x0080
//(8b) (r/w) unsigned a comparison value 62.5mg/LSB
		//initialize (2g 0x06 32x62.5mG =2g)
		
#define OFSX			0X1E00
//(8b) (r/w) 2's complement offset adjustment 15.6mg/LSB
		//calibrate
#define OFSY			0X1F00
//(8b) (r/w) 2's complement offset adjustment 15.6mg/LSB
		//calibrate
#define OFSZ			0X2000
//(8b) (r/w) 2's complement offset adjustment 15.6mg/LSB
		//calibrate		

#define DUR				0X2100
#define TWENTY_ms		0x0032
//(8b) (r/w) unsigned duration of THRES_TAP value 625 uS/LSB
		//initialize (20mS 0x20 32x625uS = 20mS)
		
#define LATENT			0X2200
#define fast			0X0080
//(8b) (r/w) unsigned duration before 2nd tap 1.25mS/LSB
		//none
#define WINDOW			0X2300
#define slow			0X00F0
//(8b) (r/w) unsigned duration before 2nd tap 1.25mS/LSB
		//none
#define THRESH_ACT  	0X2400
#define one_g			0x0010
//(8b) (r/w) unsigned a comparison value 62.5mg/LSB
		//none

#define THRESH_INACT	0X2500
#define quarter_g		0x0004

//(8b) (r/w) unsigned a comparison value 62.5mg/LSB

		//none
#define TIME_INACT		0X2600
#define four_min_15_sec 0x00FF
#define five_seconds	0x0005
//(8b) (r/w) unsigned duration before b4 inactivity declare
		//none
		
#define ACT_INACT_CTL	0X2700
#define x_y_z_inactive	0x000F
//(8b) (r/w) r - register 0x07 DC coupled and compares the current acceleration with threshold
		//initialize (0x00 diables activity and inactivity)
		
#define THRESH_FF		0X2800
#define eigth_g			0x0002
//(8b) (r/w) unsigned Free fall
		//none

#define TIME_FF			0X2900
#define twenty_ms		0x0004
#define five_ms			0x0001
//(8b) (r/w) unsigned duration free fall
		//none
		
#define TAP_AXES		0X2A00
#define x_y_n_z			0x0007
	#define supress		0x0008
	#define x_enb		0x0004
	#define y_enb		0x0002
	#define z_enb		0x0001
//(8b) (r/w) setting value to 0x07 activates x y and x axes taps
// write adxl data into ram until ball is hit
		//initialize (0x07) tap available, but setting interrupts on x, y, and z
		
#define ACT_TAP_STATUS	0X2B00
//(8b) (r) read bit 3,2,1 defines which axes was tapped 
		//read (determines which axes tapped x(D6 = 1), y (D5 = 1), or z (D4 = 1))
	#define x_tapped	0x0004
	#define y_tapped	0x0002
	#define z_tapped	0x0001
	
#define BW_RATE			0X2C00

#define BW_DATA			0x000A
//(8b) (r/w) data rate write 0x0A 100Hz no power conservation has a higher SNR
		//initialize (0x0A full power and 100 Hz sampling)

#define POWER_CTL		0X2D00
#define Link			0x0020
#define AutoSleep		0x0010
#define MEASURE			0x0008
#define SleepBit		0x0004
#define WakeUp			0x0003
#define LOW_POWER		0x003F 		//link autosleep measure 1Hz
#define NORMAL			0x003B
#define STARTUP			0x001B		//If link is set initially inactive is the woulb be startup state
#define STANDBY			0x0000
//(8b) (r/w) 0x04 measure bit set sleep measurements 8 Hz power consumed
		//initialize (0x28)no sleep and measuring normal 

#define INT_ENABLE		0X2E00
#define DATA			0x0080
#define TAP				0x0040
#define DOUBLETAP		0x0020
#define ACTIVE			0x0010
#define INACTIVE		0x0008
#define FREEFALL		0x0004
#define WATERMARK		0x0002
#define OVERRUN			0x0001
#define Wake_interrupts 0x007C
#define Live_Record 	0x00E4
#define Upload			
#define none			0x0000
//(8b) (r/w) 
		//initialize (0xC0) interrupts on data ready and sinlge taps
	
#define INT_MAP			0X2F00

#define MAP_DATA		0x0080	
//(8b) (r/w) 
		//initialize (0x80) data ready interrupt on accl_int tap on accl_2_int everything else on accl_int_1
		//initialize (0x7F) data ready interrupt on accl_2_int tap on accl_int
#define INT_SOURCE		0X3000
#define tap_x			0x4004
#define tap_y			0x4002
#define tap_z			0x4001
#define taptap_x		0x6004
#define taptap_y		0x6002
#define taptap_z		0x6001
#define activity		0x1000
#define inactivity		0x0800
#define freefall		0x0400
#define asleep			0x0008

#define which_int		0x7C	//making only the sources relavent available in switch statement (INT1)
								//0x7C00 makes tap, tap tap, activity, inactivity and freefall relavent
#define which_axs		0x07	//making only the sources relavent available in switch statement (INT1)
//(8b) (r) reading a 0x80 data ready interrupt )x 40 a single tap occured
		//read bit D7 = 1 data ready D6 = 1 tap read act tap status to determine which axes
  
#define DATA_FORMAT		0X3100
//(8b) (r/w) write 0x00 puts device in 10 bit data right justified 2g range
		//initailize (0x00)placing device in 4-wire communication

#define FULL_RES		0x000B
//(8b) (r/w) write 0x0F puts device in 13 bit data left justified 16g range

#define DATAX0			0X3200
//(8b) (r) 2's compliment high bit?
		//read data after data ready interrupt
 
#define DATAX1			0X3300
//(8b) (r) 2's compliment low bit?
		//read data after data ready interrupt

#define DATAY0			0X3400
//(8b) (r) 2's compliment high bit?
		//read data after data ready interrupt

#define DATAY1			0X3500
//(8b) (r) 2's compliment low bit?
		//read data after data ready interrupt

#define DATAZ0			0X3600
//(8b) (r) 2's compliment high bit?
		//read data after data ready interrupt

#define DATAZ1			0X3700
//(8b) (r) 2's compliment low bit?
		//read data after data ready interrupt

#define FIFO_CTL		0X3800
#define FIFO_DATA		0x0000
//(8b) (r/w) setting 0x1F bypasses the fifo
//           setting 0x80 streams data through fifo
		//initialize (0x1F)

#define FIFP_STATUS		0X3900

//***********************************************************************//
//*****************   PIC24FJ64GB004/ADXL345 INTERFACE   ****************//
//***********************************************************************//
// SPI INPUTS AND OUTPUTS
#define ADXL345_SDI		0b01010		//Maps SPI2_SDO onto RP3 (function(A) mapps SDO on SPI MCU perspective)
#define ADXL345_SCK 	0b01011		//Maps SPI2_SCK onto RP2 
#define SS_ACCL			_LATB4
#define ADXL345_SDO 	0b10000		//Map RP16 onto SPI2_SDI 


// peripheral configuration
#define SPI_MASTER_ADXL	0x047E 							//Check the Datasheet 39940c page 172 (16 bit ops)
#define SPI_ENABLE_ADXL	0x8000							//endable spi port clear status

#endif /* INERTIAL_DATA_H */
