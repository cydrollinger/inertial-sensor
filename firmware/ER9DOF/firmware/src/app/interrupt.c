//***********************************************************************//
//********************   Interrupts  ************************************//
//***********************************************************************//

#include <NineDOF.h>
#include <interrupt.h>
#include <system.h>
//#include "HardwareProfile.h"

extern int bytesOdata; 
extern uint8_t i2cComSend[], i2cComReceive[],sampleNumber;
extern bool i2cInterrupt,dataReady;
extern void InitializeUSB();

void INITIALIZE_interrupts (void)
{
INTCON1 = 0x0000;							//setting nested interrupts bit 15 = 0

//Mapping the MPU-9150 interrupt into the p24FJ64GB004
_INT1R = 0x02;								//SIGNAL INT MPU-5190 RP2/RB2   pin 	23 (0x02 = RP2)
									//PLACING PIN 23 AS THE EXTERNAL INTERRUPT 1 SIGNAL
// Clear and Enable Interrupts
_INT1IF = off;								//CLEARING BEFORE ENABLING(IFS1) tap
_INT1IE = on;								//IS RP2 SET THIS IS ENABLING(IEC1)tap
//_INT1IP =0x05;

T2CON = 0x8030;								//32 bit timer with a one to one clock with the system clock
PR2 = 0xF424;
_T2IF = off;
_T2IE = on;

_MI2C1IP = 0x05;
_MI2C1IF = off;
_MI2C1IE = on;

_CN21IE = on;
//_CN0IE  = on;
_CNIF = off;
_CNIE = on;
}


/********************************************************************
 * Function:        void _ISR _INT2Interrupt(void)
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        Once Int1 interrupt occurs read ACT_TAP_STATUS
 *					
 * Note:            None
 *******************************************************************/
void __attribute__((interrupt, no_auto_psv)) _INT1Interrupt (void)
{
sampleNumber++;
//if (sampleNumber == 1)
dataReady = TRUE;
_INT1IF = off;
}//_INT1Interrupt Vector #20

/********************************************************************
 * Function:        void 
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            None
 *******************************************************************/
void __attribute__((interrupt, no_auto_psv))  _MI2C1Interrupt (void)
{
i2cInterrupt = TRUE;
_MI2C1IF = off;
}

void __attribute__((interrupt, no_auto_psv))  _T2Interrupt (void)
{

_T2IF = off;
}

void __attribute__((interrupt, no_auto_psv))  _CNInterrupt (void)
{
    _CNIF = off;
}

