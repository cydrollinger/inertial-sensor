/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
LEDS
	GREEN anode35 cathode34
	RED	  anode34 cathode35		
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2
Green Led
Red Led

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

#include <HardwareProfile.h>
#include "er_it_io.h"



void INITIALIZE_LEDS(void)
{

_TRISA1 = OUTPUT_PIN;
_TRISB0 = OUTPUT_PIN;
_TRISB1 = OUTPUT_PIN;

_RP6R = red_pwm;							//Seifert
//_RP1R = red_pwm; swim
_RP0R = green_pwm;
_RP1R = blue_pwm;							//Seifert
//_RP6R = blue_pwm; swim

/* BLUE*/
OC1R = off;									//duty cycle (0% - 0x000)
OC1RS = one_hundred_Hz;						//period based on 16MHz clock and prescaler = 8
OC1CON2bits.SYNCSEL = sync_source;			//source is the output capture itself
OC1CON2bits.OCTRIG = zero;
OC1CON1bits.OCTSEL = tim_1;
OC1CON1bits.OCM = pwm;						//OCM enables the pwm and disables

/*GREEN*/
OC2R = off;									//duty cycle (0% - 0x0000)
OC2RS = one_hundred_Hz;						//period based on 16MHz clock and prescaler = 8
OC2CON2bits.SYNCSEL = sync_source;			//source is the output capture itself
OC2CON2bits.OCTRIG = zero;
OC2CON1bits.OCTSEL = tim_1;
OC2CON1bits.OCM = pwm;						//OCM enables the pwm and disables

/*RED*/
OC3R = off;									
OC3RS = one_hundred_Hz;						//period based on 16MHz clock and prescaler = 8
OC3CON2bits.SYNCSEL = sync_source;			//source is the output capture itself
OC3CON2bits.OCTRIG = zero;
OC3CON1bits.OCTSEL = tim_1;
OC3CON1bits.OCM = pwm;						//OCM enables the pwm and disables

T1CON = 0x8010;								//change was 0x8000 but useful for OC PWM so prescaler by 8 100Hz PWM
PR1 = one_hundred_Hz;						//T2 timer

GREEN_on(less_than_apercent);
}

void BLUE_on(int duty)
{
OC1R =	duty;
OC2R =	off;								//
OC3R =	off;								//duty cycle (10% - 7CF)     (1% - CF) 
}

void GREEN_on(int duty)
{
OC1R =	off;
OC2R =	duty;
OC3R =	off;
}

void RED_on(int duty)
{
OC1R =	off;
OC2R =	off;
OC3R =	duty;								//duty cycle (10% - 7CF)     (1% - CF) 
}

void ORANGE_on(int duty)
{
OC1R = off;
OC2R = duty;
OC3R = duty;
}

void PURPLE_on(int duty)
{
OC1R = duty;
OC2R = off;
OC3R = duty;
}

void AQUA_on(int duty)
{
OC1R = duty;
OC2R = duty;
OC3R = off;
}

void LEDs_on(int duty)
{
OC1R = duty;
OC2R = duty;
OC3R = duty;
}

int is_RED_on (void)
{
return(OC3R);
}

int is_GREEN_on (void)
{
return(OC2R);
}

int is_BLUE_on (void)
{
return(OC1R);
}

