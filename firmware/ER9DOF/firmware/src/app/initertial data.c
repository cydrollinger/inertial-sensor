/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

#include <inertial data.h>
#include "HardwareProfile.h"

extern void itoa(int, char *);
char InertialData[FullPage], *InertialDataPtr = InertialData, sign = 32;
int pseudoData = 0;


/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            ADC/Gyroscope together a module ADC initialized running (power consumption)
 *					Gyroscope is initialized as low power call powerup to use!
 *****************************************************************************/

void INITIALIZE_ADC(void) 		      						//describes the pin function analog or digital all digital except AN(9-10)
{
_TRISA7 		= OUTPUT_PIN;
_TRISA10		= OUTPUT_PIN;

GYRO_POWERUP();

AD1PCFG = AINPUTS;																					
AD1CON1 = 0x00E0;											//automatic conversion start after sampling and unsigned integer 16 bit
AD1CON2 = zero;												//AVSS and AVDD are ref+/-	
AD1CON3 = 0x1F02;											//Tsamp(= 4 Us) = 32 x Tad; Tad = 2 x Fcy(16MHz) = 125ns
AD1CSSL = zero;												//no scanning
AD1CON1bits.ADON = on;										//ADC on
}//end. init_ADC

int read_ADC (int ch)
{
AD1CHS  = ch;												//sets MUXA as channel 10
AD1CON1bits.SAMP = on;
while(!AD1CON1bits.DONE);
return ADC1BUF0;
} //end. read_ADC

void GYRO_SHUTDOWN (void)
{
Self_Test = on;
reset_HPF = on;
}

void GYRO_POWERUP (void)
{
Self_Test = off;
reset_HPF = off;
}
//***********************************************************************//
//******************** ADXL345 SPI  *************************************//
//***********************************************************************//


/******************************************************************************
 * Function:        void INITIALIZE_adxl345
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            accelerometer is initialized as low power call ADXL_POWER_CTL to use!
 *****************************************************************************/
void INITIALIZE_adxl345(void)
{
//Intialize p24FJ64GB004 for the ADXL345 Accelerometer on SPI2
//mapping function to output
_RP2R	= 	ADXL345_SCK;								//SPI2_SCK at RP2  pin 	23 
_RP3R 	=	ADXL345_SDI;								//SPI2_SDO to RP3  pin 	24 
_TRISB4 = 	OUTPUT_PIN;									//SS_ACCL chip select 

SS_ACCL = on;											//deselect the ADXL345

//mapping input to function
RPINR22 = 	ADXL345_SDO;								//RP16 pin 25 to SPI2_SDI 
_TRISC2 = INPUT_PIN;									//RC2 port input pin#27(AN8) int_accl TAP interrupt controlled by TAP_THRES ADX345.h
_TRISC1 = INPUT_PIN;									//RC1 port input pin#26(AN7) int_2_accl DATA interrupt controlled by BW_DATA ADXL345.h

//settting SPI2 registers
SPI2CON1 = SPI_MASTER_ADXL;								//select mode
SPI2STATbits.SPIROV = off;								//clear no overflow spi buffer
SPI2STAT = SPI_ENABLE_ADXL;								//enable peripheral

//Initialize ADXL345

//Register
SS_ACCL = off;
write_adxl345(WRITE_BYTE | DATA_FORMAT 	| FULL_RES);				//4-wire SPI [0x3108]
write_adxl345(WRITE_BYTE | THRES_TAP 	| EIGHT_g );				//initialize two g threshold tap [0x1D20]
write_adxl345(WRITE_BYTE | DUR 			| TWENTY_ms );				//
write_adxl345(WRITE_BYTE | LATENT		| fast );
write_adxl345(WRITE_BYTE | WINDOW		| slow );

write_adxl345(WRITE_BYTE | THRESH_ACT	| 0x000A );					//1 g shake awake

write_adxl345(WRITE_BYTE | THRESH_INACT	| 0x0002 );					// places device on the alert for inactive readings
write_adxl345(WRITE_BYTE | TIME_INACT	| 0x000A );					// 0x40 1 minute 4 seconds
write_adxl345(WRITE_BYTE | ACT_INACT_CTL| 0x00FF );					//DC coupled in/active x, y, and z
write_adxl345(WRITE_BYTE | THRESH_FF	| eigth_g);					//
write_adxl345(WRITE_BYTE | TIME_FF      | five_ms);					//

write_adxl345(WRITE_BYTE | TAP_AXES 	| x_y_n_z);					//interrupts enabled on x y and z [0x2A07]
write_adxl345(WRITE_BYTE | BW_RATE 		| BW_DATA );				//BW 50Hz [0x2C0A]
write_adxl345(WRITE_BYTE | POWER_CTL	| MEASURE);					//Normal operation hi power greater SNR [0x2D28]
write_adxl345(WRITE_BYTE | INT_MAP 		| MAP_DATA);				//puts data ready on int_accl and tap axes on ACCL_2_INT (HW) [0x2FBF]
write_adxl345(WRITE_BYTE | INT_ENABLE 	| Live_Record);	 				//enabling data ready int2 and tap detection int1  [0x2EC0]
write_adxl345(READ_BYTE  | INT_SOURCE );							//clears any inadvertant interrupt [0xB000]
write_adxl345(READ_MB | DATA_FORMAT);
write_adxl345(0x0000);
write_adxl345(0x0000);
write_adxl345(0x0000);
SS_ACCL = on;


}//end int_adxl345

/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
int write_adxl345(int data)								//int is 16 bits
{
// send one byte of data and receive one back at the same time
    SPI2BUF = data;										// write to buffer for TX
    while( !SPI2STATbits.SPIRBF);						// wait for transfer to complete
    return (SPI2BUF );    								// read the received value
}//write_accelerometer

/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
int Read_Which_Interrupt(void)
{
union PackByte interrupt_axis;
interrupt_axis.word = 0x0000;

SS_ACCL	= off;
interrupt_axis.LowByte = write_adxl345(READ_BYTE  | ACT_TAP_STATUS );
interrupt_axis.LowByte = interrupt_axis.LowByte & which_axs;
interrupt_axis.HighByte = write_adxl345(READ_BYTE  | INT_SOURCE );
interrupt_axis.HighByte = interrupt_axis.HighByte & which_int;
SS_ACCL	= on;

if (interrupt_axis.bit_13)
interrupt_axis.word = activity;
if (interrupt_axis.bit_12)
interrupt_axis.word = inactivity;

return(interrupt_axis.word);

}//

/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
void Read_Inertial_data(char *ptr)
{
union PackByte temp,endian;

temp.word					= read_ADC(gyro_diffp);
endian.HighByte				= temp.HighByte;
endian.LowByte				= temp.LowByte;
endian.word					= TwosComplement(endian.word);
*(ptr)						= sign;
itoa(endian.word,(ptr+1));
*(ptr+6)					= 44;

SS_ACCL						= off;
write_adxl345(READ_MB | DATA_FORMAT);
temp.word					= write_adxl345(0x0000);
endian.LowByte				= temp.HighByte;
endian.HighByte				= temp.LowByte;
endian.word					= TwosComplement(endian.word);
*(ptr+7)					= sign;
itoa(endian.word,(ptr+8));
*(ptr+13) 					= 44;

temp.word					= write_adxl345(0x0000);
endian.LowByte				= temp.HighByte;
endian.HighByte				= temp.LowByte;
endian.word					= TwosComplement(endian.word);
*(ptr+14)					= sign;
itoa(endian.word, (ptr+15));
*(ptr+20) 					= 44;

temp.word					= write_adxl345(0x0000);
endian.LowByte				= temp.HighByte;
endian.HighByte				= temp.LowByte;
endian.word					= TwosComplement(endian.word);
*(ptr+21)					= sign;
itoa(endian.word, (ptr+22));
*(ptr+27) 					= 10;
SS_ACCL						= on;


////temp.word					= read_ADC(gyro_diffp);
////*(ptr)						= temp.LowByte;
////*(ptr + 1)					= temp.HighByte;
//SS_ACCL						= off;
//write_adxl345(READ_MB | DATA_FORMAT);
//temp.word					= write_adxl345(0x0000);
////*(ptr + 2)					= temp.HighByte;
////*(ptr + 3)					= temp.LowByte;
//temp.word					= write_adxl345(0x0000);
////*(ptr + 4)					= temp.HighByte;
////*(ptr + 5)					= temp.LowByte;
////*(ptr + yoffset)			=ReverseBytes.word;
//temp.word					= write_adxl345(0x0000);
////*(ptr + 6)					= temp.HighByte;
////*(ptr + 7)					= temp.LowByte;
//SS_ACCL						= on;
//
//itoa(pseudoData++, ptr);
//*(ptr+5) 	= 10;
//
//itoa(pseudoData++, (ptr+6));
//*(ptr+11) 	= 10;
//
//itoa(pseudoData++, (ptr+12));
//*(ptr+17) 	= 10;
//
//itoa(pseudoData++, (ptr+18));
//*(ptr+23) 	= 10;
}


/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
void ADXL_POWER_CTL(int PowerState)
{
SS_ACCL = off;
write_adxl345(WRITE_BYTE | POWER_CTL 	| PowerState );
SS_ACCL = on;
}
/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
void ADXL_Enable_Interrupts(int which_interrupts)
{
SS_ACCL = off;
write_adxl345(WRITE_BYTE | INT_ENABLE 	| which_interrupts);
SS_ACCL = on;
}

/******************************************************************************
 * Function:        void INITIALIZE_ADC
 * PreCondition:    None
 * Input:           None
 * Output:          None
 * Side Effects:    None
 * Overview:        
 * Note:            state reflects the accelerometer operation
 *****************************************************************************/
int TwosComplement(int integer)
{

union PackByte ones;
ones.word = integer;
sign = 32;
if((ones.bit_16))
{
	sign = 45;
	ones.word = ~integer;
	ones.word++;
}
return(ones.word);
}
