/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

 #ifndef INTERRUPT_H 
 #define INTERRUPT_H

void INITIALIZE_IO(void);
void INITIALIZE_interrupts(void);

#endif /* INTERRUPT_H */
