/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *******************************************************************/

#ifndef SYSTEM_H
#define SYSTEM_H

#include <p24FJ64GB004.h>
#include <stdbool.h>

//#include <buttons.h>
//#include <leds.h>
//#include <adc.h>

#include <io_mapping.h>
//#include <power.h>

#include <drv_spi.h>

#define FALSE                               0
#define TRUE                                1
#define OUTPUT                              0
#define INPUT                               1
#define off                                 0
#define on                                  1

union PackByte
{
 uint16_t word;
 struct
 	{
    uint8_t LowByte;
    uint8_t HighByte;
  	};
struct
	{
	unsigned bit_1 :1;
	unsigned bit_2 :1;
	unsigned bit_3 :1;
	unsigned bit_4 :1;
	unsigned bit_5 :1;
	unsigned bit_6 :1;
	unsigned bit_7 :1;
	unsigned bit_8 :1;
	unsigned bit_9 :1;
	unsigned bit_10 :1;
	unsigned bit_11 :1;
	unsigned bit_12 :1;
	unsigned bit_13 :1;
	unsigned bit_14 :1;
	unsigned bit_15 :1;
	unsigned bit_16 :1;
	};
 };
 
//the pin connected to VUSB
//#define VBUS


// Definition for system clock
#define SYS_CLK_FrequencySystemGet()        16000000
// Definition for peripheral clock
#define SYS_CLK_FrequencyPeripheralGet()    SYS_CLK_FrequencySystemGet()
// Definition for instruction clock
#define SYS_CLK_FrequencyInstructionGet()   (SYS_CLK_FrequencySystemGet() )

#define MAIN_RETURN int

//Definitions letting SPI driver code know which SPI module to use
#define DRV_SPI_CONFIG_CHANNEL_1_ENABLE      // Enable SPI channel 1
//#define DRV_SPI_CONFIG_CHANNEL_2_ENABLE      // Enable SPI channel 2
//#define DRV_SPI_CONFIG_CHANNEL_3_ENABLE      // Enable SPI channel 3
//#define DRV_SPI_CONFIG_CHANNEL_4_ENABLE      // Enable SPI channel 4

#define USER_SPI_MODULE_NUM   1			//MSSP number used on this device

/*********************************************************************
* Function: void SYSTEM_Initialize( SYSTEM_STATE state )
*
* Overview: Initializes the system.
*
* PreCondition: None
*
* Input:  SYSTEM_STATE - the state to initialize the system into
*
* Output: None
*
********************************************************************/
void SYSTEM_Initialize( void );


/*********************************************************************
* Function: void SYSTEM_Tasks(void)
*
* Overview: Runs system level tasks that keep the system running
*
* PreCondition: System has been initalized with SYSTEM_Initialize()
*
* Input: None
*
* Output: None
*
********************************************************************/
#define SYSTEM_Tasks()

// User-defined function to set the chip select for our example drive
void USER_SdSpiSetCs_1 (uint8_t a);
// User-defined function to get the card detection status for our example drive
bool USER_SdSpiGetCd_1 (void);
// User-defined function to get the write-protect status for our example drive
bool USER_SdSpiGetWp_1 (void);
// User-defined function to initialize tristate bits for CS, CD, and WP
void USER_SdSpiConfigurePins_1 (void);


#endif //SYSTEM_H
