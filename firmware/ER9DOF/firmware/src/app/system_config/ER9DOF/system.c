/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *******************************************************************/

#include <p24FJ64GB004.h>
#include <system.h>
#include <system_config.h>
#include <usb.h>
#include "fileio.h"
#include "sd_spi.h"
#include <leds.h>
#include <buttons.h>
#include <adc.h>
#include <NineDOF.h>
#include <interrupt.h>


/** CONFIGURATION Bits **********************************************/

//_CONFIG1(WDTPS_PS1 & FWPSA_PR32 & WINDIS_OFF & FWDTEN_OFF & ICS_PGx1 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
//
//_CONFIG2(POSCMOD_XT & I2C1SEL_SEC & IOL1WAY_OFF & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV4 & IESO_OFF)
//
//_CONFIG3(WPFP_WPFP0 & SOSCSEL_IO & WUTSEL_LEG & WPDIS_WPDIS & WPCFG_WPCFGDIS & WPEND_WPENDMEM)
//
//_CONFIG4(DSWDTPS_DSWDTPS3 & DSWDTOSC_LPRC & RTCOSC_SOSC & DSBOREN_OFF & DSWDTEN_OFF)

extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameter;
 
/*********************************************************************
* Function: void SYSTEM_Initialize( SYSTEM_STATE state )
*
* Overview: Initializes the system.
*
* PreCondition: None
*
* Input:  SYSTEM_STATE - the state to initialize the system into
*
* Output: None
*
********************************************************************/
void SYSTEM_Initialize( void )
{
    unsigned int pll_startup_counter = 600;

    AD1PCFG = 0xFFFF;

    //On the PIC24FJ64GB004 Family of USB microcontrollers, the PLL will not power up and be enabled
    //by default, even if a PLL enabled oscillator configuration is selected (such as HS+PLL).
    //This allows the device to power up at a lower initial operating frequency, which can be
    //advantageous when powered from a source which is not gauranteed to be adequate for 32MHz
    //operation.  On these devices, user firmware needs to manually set the CLKDIV<PLLEN> bit to
    //power up the PLL.
    CLKDIVbits.PLLEN = 1;
    while(pll_startup_counter--);

    //Device switches over automatically to PLL output after PLL is locked and ready.


//	The USB specifications require that USB peripheral devices must never source
//	current onto the Vbus pin.  Additionally, USB peripherals should not source
//	current on D+ or D- when the host/hub is not actively powering the Vbus line.
//	When designing a self powered (as opposed to bus powered) USB peripheral
//	device, the firmware should make sure not to turn on the USB module and D+
//	or D- pull up resistor unless Vbus is actively powered.  Therefore, the
//	firmware needs some means to detect when Vbus is being powered by the host.
//	A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
// 	can be used to detect when Vbus is high (host actively powering), or low
//	(host is shut down or otherwise not supplying power).  The USB firmware
// 	can then periodically poll this I/O pin to know when it is okay to turn on
//	the USB module/D+/D- pull up resistor.  When designing a purely bus powered
//	peripheral device, it is not possible to source current on D+ or D- when the
//	host is not actively providing power on Vbus. Therefore, implementing this
//	bus sense feature is optional.  This firmware can be made to use this bus
//	sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
//	HardwareProfile.h file.
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN;
    #endif

//	If the host PC sends a GetStatus (device) request, the firmware must respond
//	and let the host know if the USB peripheral device is currently bus powered
//	or self powered.  See chapter 9 in the official USB specifications for details
//	regarding this request.  If the peripheral device is capable of being both
//	self and bus powered, it should not return a hard coded value for this request.
//	Instead, firmware should check if it is currently self or bus powered, and
//	respond accordingly.  If the hardware has been configured like demonstrated
//	on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
//	currently selected power source.  On the PICDEM FS USB Demo Board, "RA2"
//	is used for	this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
//	has been defined in HardwareProfile.h, and that an appropriate I/O pin has been mapped
//	to it in HardwareProfile.h.
    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;
    #endif

    //********* Initialize Peripheral Pin Select (PPS) *************************
    //  This section only pertains to devices that have the PPS capabilities.
    //    When migrating code into an application, please verify that the PPS
    //    setting is correct for the port pins that are used in the application.
    //Initialize the SPI
    RPINR20bits.SDI1R = 25;     //MSDI
    RPOR11bits.RP23R = 7;        //MSDO
    RPOR12bits.RP24R = 8;        //SCK

    //enable a pull-up for the card detect, just in case the SD-Card isn't attached
    //  then lets have a pull-up to make sure we don't think it is there.
    CNPU1bits.CN4PUE = 0;
    CNPU1bits.CN5PUE = 0;
    //enable a pull-up on the switch BUTTON_S3 or NINE_DOF_BOTTON
    //CNPU1bits.CN0PUE = 0;
    _USBEN = 1;					//enables highspeed USB through D+ pullup
    _USBPWR = 1;
    //LTC3558 High Power enable
    TRISBbits.TRISB4 = OUTPUT;
    hipower = on;

    INITIALIZE_interrupts();
    INITIALIZE_MPU9150();
    
    INITIALIZE_LEDs();
    
    TRISAbits.TRISA4 = INPUT;               //button

    TRISBbits.TRISB9 = INPUT;               //VBUS 5v tolerant
    
    TRISAbits.TRISA8 = INPUT;               //Charge

}

void USER_SdSpiConfigurePins_1 (void)
{
    // Deassert the chip select pin
    LATCbits.LATC6 = 1;
    // Configure CS pin as an output
    TRISCbits.TRISC6 = 0;
    // Configure CD pin as an input
    TRISBbits.TRISB0 = 1;
    // Configure WP pin as an input
    TRISBbits.TRISB1 = 1;
}

inline void USER_SdSpiSetCs_1(uint8_t a)
{
    LATCbits.LATC6 = a;
}

inline bool USER_SdSpiGetCd_1(void)
{
    return (!PORTBbits.RB0) ? true : false;
}

inline bool USER_SdSpiGetWp_1(void)
{
    return (PORTBbits.RB1) ? true : false;
}

#if defined(USB_INTERRUPT)
void __attribute__((interrupt,auto_psv)) _USB1Interrupt()
{
    USBDeviceTasks();
}
#endif

