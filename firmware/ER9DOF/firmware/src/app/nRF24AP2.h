/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
ANT+ on UART	
	44			RP8			UART_TX
	43			RP7			UART_RX
	41			RB5			suspend
	38			RC5/RP21	sleep
	37			RC4/RP20	RTS
	36			RC3/RP19	RESET	
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 - 128
Nordic Semiconductor ANT+ : nRF24AP2

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

 #ifndef nRF24AP2_H 
 #define nRF24AP2_H

#define ant_RTS		_RC4
#define ant_tx 		0x0008			//Maps U1Rx into RP8 through RPINR18(UART INPUTS)
#define ant_rx 		0x03			//Maps U1Tx onto RP7 function(3) through RPO3 register


#define ant_reset 	_LATC3			//asserted when low (active low)
#define ant_sleep 	_LATC5			//asserted when high (active high)
#define ant_susp 	_LATB5			//asserted when low (active low)

#define max_send 			0x0D				//the largest data packet to send via ANT+ is 13
#define Ant_Sync			0xA4
#define ant_channel_number	0x00
#define channel_live		0x01

#define zerodBm				0x03
#define neg5dBm				0x02
#define neg10dBm			0x01
#define neg20dBm			0x00

#define burst				0x01
#define acknowledge			0x00


/*******
UART1 configuration
*******/							//Fcy=Fosc/2 UxBRG = (Fcy/(4*baud rate))-1, DS39708B EQ. 21-2
									//%err = .8% EQ. 21-1 
#define BRATE 		0x45			//Baud rate = 57600 bits/sec nRF24AP2 datasheet pg. 25 table 5 br0=1, br1=1, br2=1 69 = 0x45
#define U1_ENABLE  0x8008			//UARTTx and UARTRx controlled while RTS and CTS latched in ports  
									//enable UART1, BREGH=1, 1stop, no parity

#define U_TX 	0x0400				//enable transmission, clear all flags

void INITIALIZE_ANT(void);
void ANT_LIVE(void);
void ANT_IDLE(void);
void ANT_ResetSystem(void);
void ANT_UnassignChannel(int channel[]);
void ANT_AssignChannel(int channel[]);
void ANT_SetChannelPeriod(int period[]);
void ANT_SetChannelRFFreq(int frequency[]);
void ANT_SetTransmitPower(int power);
void ANT_SetNetworkKey(int key[]);
void ANT_SetChannelId(int id[]);
void ANT_OpenChannel(int channel[]);
void ANT_CloseChannel(int channel[]);
void ANT_SendBroadcastData(int channel[], int *ptr);
void ANT_SendAcknowledgeData(int channel[], int *ptr);
void ANT_send_burst_data(int channel[], int *ptr);

#endif /* nRF24AP2_H  */
