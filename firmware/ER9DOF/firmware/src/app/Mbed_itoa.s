/********************************************************************
 Converts an unsigned integer N in w0 to a 5-byte ASCII numerical string [w4]
 using a reciprocal multiplication and fractional conversion techniques
 19 clocks including RETURN, 17 program words
 Registers trashed: w0..w4
********************************************************************/
.global _my_itoa
_my_itoa:
rec10k = 429497         		; =~ 2^32/10000
	mov		w1,w4
	
	mov    #rec10k >> 16,w1
    mul.uu    w1,w0,w2    		; w3:w2 = partial product MSWs
    mov    #rec10k & 0xFFFF,w1
    mul.uu    w1,w0,w0    		; w1:w0 = partial product LSWs

    add    w1,w2,w0    			; w0 = fract(N%10000)
    mov    #'0',w2        		; w2 = ASCII bit mask
    addc.b    w3,w2,[w4++]    	; w3 = N/10000, store an ASCII MS character

    inc    w0,w0        		; Correct the remainder to use 16-bit ops

    mul.uu    w0,#10,w0    		; w1 = next ASCII digit (0..9 range), w0 = fractional remainder
    ior.b    w1,w2,[w4++]   	; Convert to ASCII and store a next character

    mul.uu    w0,#10,w0    		; Same as above
    ior.b    w1,w2,[w4++]    	;

    mul.uu    w0,#10,w0    		; Same as above
    ior.b    w1,w2,[w4++]    	;

    mul.uu    w0,#10,w0    		; Same as above
    ior.b    w1,w2,[w4++]    	;
			

    return
.end

