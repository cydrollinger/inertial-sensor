/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/

#include <nRF24AP2.h>
#include <er it.h>

//int ant_key[] 			= {0x00,0xB9,0xA5,0x21,0xFB,0xBD,0x72,0xC3,0x45};
//int ant_id[] 				= {ant_channel_number,0x73,0x80,0x01};
int ant_id[] 				= {ant_channel_number,0x00,0x00,0x00,0x00};		//5 devices 5 numbers 
int ant_id_live[] 			= {channel_live,0x00,0x00,0x00,0x00};
//int ant_frequency[] 		= {ant_channel_number,0x42};					//2.466 GHz
//int ant_frequency[] 		= {ant_channel_number,0x39};					//	
int ant_frequency[] 		= {ant_channel_number,0x0A};					//2.41 GHz
//int ant_period[] 			= {ant_channel_number,0xAC,0x00};				//172 Hz
int ant_period_live[] 		= {channel_live,0x48,0x01};				        //100Hz
//int ant_period[] 			= {ant_channel_number,0x44,0x04};				//30 Hz
//int ant_period[] 			= {ant_channel_number,0x3D,0x0A};				//12.5 Hz
int ant_period[] 			= {ant_channel_number,0x20,0x00};				//4 Hz 8192/32763 = .25
int ant_channel[] 			= {ant_channel_number,0x10,0x00};
int ant_channel_live[]		= {channel_live,0x10,0x00};
int ant_power				= zerodBm;										//0: -20dBm, 1: -10dBm 2: -5dBm, 3: 0dBm

int buffer[max_send] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
int buffer_index =0x40;
int init_buffer[0x08] = {0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80};


/*********
nRF24AP2
*********/
/****************************************************************
These are like funtions calls placed on the serial communication.
The functions have a sync byte, message length, a message call, 
data, and a checksum.
****************************************************************/

void INITIALIZE_ANT(void)
{
	_TRISC3		= OUTPUT_PIN;	//places RC3 as output ANT RESET pin
	_TRISB5		= OUTPUT_PIN;	//places RB5 as output ANT Suspend pin
	_TRISC5		= OUTPUT_PIN;	//places RC5 as output ANT Sleep pin
		
	ant_reset   = one;
	ant_reset   = zero;
	ant_reset   = one;
	
	ant_susp	= one;			//aserting ant_susp causes a reset when in idle
	ant_sleep   = zero;

	U1BRG 		= BRATE;		//initialize the baud rate generator
	U1MODE	 	= U1_ENABLE;	//initialize the UART module
	U1STA 		= U_TX;			//enable the transmitter
	_RP7R		= ant_rx;
	RPINR18 	=(ant_tx);
  	_TRISC4		= INPUT_PIN;
	_BRGH 		= 1;
	
	ANT_AssignChannel(ant_channel_live);		//0x42		//Change channel to 0x01 (One)
	ANT_SetChannelId(ant_id_live);				//0x51
	ANT_SetChannelPeriod(ant_period_live);		//0x43		//Change period to ~100Hz (ANT in IDLE STATE)
	//ant_sleep = one;
	ANT_OpenChannel(ant_channel_live);	    //0x4B		//Change channel to 0x01 (One)
	//ANT_SendBroadcastData(ant_channel_live,init_buffer);
	//ANT_IDLE();
}//INIT_ANT


void ANT_IDLE(void)
{
ANT_CloseChannel(ant_channel_live);
ant_sleep = one;
}

void ANT_LIVE(void)
{
ant_sleep = zero;
ANT_OpenChannel(ant_channel_live);			//0x4B		//Change channel to 0x01 (One)
}


int put_ANT (int c)
{
while(ant_RTS);
while(U1STAbits.UTXBF);
U1TXREG = c;
return c;
}//putU1

void SEND_ANT(int buffer[])
{
	int i, Length = buffer[1], data_checksum = 0x00;
		
		put_ANT(buffer[0]);						//SYNC
		put_ANT(buffer[1]);						//LENGTH
		put_ANT(buffer[2]);						//MESSAGE ID 	
				
		for(i = 0; i < Length; i++)		
		{
		data_checksum = data_checksum ^ buffer[i+3];
		put_ANT(buffer[i+3]);					//DATA
		}

		put_ANT((buffer[0]^buffer[1]^buffer[2]^data_checksum));			//CHECKSUM
		while(!ant_RTS);						//prevents successive writes before a single RTS	
}

void ANT_ResetSystem(void)
{ 		
		buffer[0] = 0xA4;						//SYNC
		buffer[1] = 0x01;						//LENGTH
		buffer[2] = 0x4A;						//MESSAGE ID Reset 	
		buffer[3] = 0x00;						//DATA1
				
		SEND_ANT(buffer);	
}

void ANT_UnassignChannel(int channel[])
{
		buffer[0] = 0xA4;						//SYNC
		buffer[1] = 0x01;						//LENGTH
		buffer[2] = 0x41;						//MESSAGE ID 	Unassign Channel
		buffer[3] = channel[0];					//DATA1 Channel Number
			
		SEND_ANT(buffer);		
}

void ANT_AssignChannel(int channel[])
{
		buffer[0] = 0xA4;						//SYNC
		buffer[1] = 0x03;						//LENGTH
		buffer[2] = 0x42;						//MESSAGE ID 	Assign Channel
		buffer[3] = channel[0];					//DATA1 Channel Number
		buffer[4] = channel[1];					//DATA2 Tx channel
		buffer[5] = channel[2];					//DATA3 Network Number (0 for public)
	
		SEND_ANT(buffer);		
}

void ANT_SetChannelPeriod(int period[])
{
		buffer[0] = 0xA4;						//SYNC
		buffer[1] = 0x03;						//LENGTH
		buffer[2] = 0x43;						//MESSAGE ID 	Set Channel Period
		buffer[3] = period[0];					//DATA1 Channel Number
		buffer[4] = period[1];					//DATA1 Channel Period(default 8192 bike power = 8102)litle endian
		buffer[5] = period[2];					//DATA2 Tx channel
			
		SEND_ANT(buffer);		
}

void ANT_SetChannelRFFreq(int frequency[])
{
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x02;						//LENGTH
		buffer[2] =0x45;						//MESSAGE ID 	Open Channel
		buffer[3] =frequency[0];				//DATA1 Channel Number
		buffer[4] =frequency[1];				//DATA2 Channel frequency default 2466 channel freq = 2.4Ghz + Xx1Mhz
		
		SEND_ANT(buffer);
}

void ANT_SetTransmitPower(int power)
{
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x02;						//LENGTH
		buffer[2] =0x47;						//MESSAGE ID 	Set Transmit Power
		buffer[3] =0x00;						//filler must be 0x00
		buffer[4] =ant_power;					//0: -20dBm, 1: -10dBm 2: -5dBm, 3: 0dBm 
		
		SEND_ANT(buffer);
}


void ANT_SetNetworkKey(int key[])
{
		buffer[0] = 0xA4;						//SYNC
		buffer[1] = 0x09;						//LENGTH
		buffer[2] = 0x46;						//MESSAGE ID 	SetNetworkKey
		buffer[3] = key[0];						//DATA1 Network Number
		buffer[4] = key[1];						//Key #1 Located in text document in C:\work\Proposals\GURU\Heavey Powder\product\ant+
		buffer[5] = key[2];						//Key #2
		buffer[6] = key[3];						//Key #3
		buffer[7] = key[4];						//Key #4
		buffer[8] = key[5];						//Key #5
		buffer[9] = key[6];						//Key #6
		buffer[10] =key[7];						//Key #7
		buffer[11] =key[8];						//Key #8

		SEND_ANT(buffer);
}

void ANT_SetChannelId(int id[])
{
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x05;						//LENGTH
		buffer[2] =0x51;						//MESSAGE ID 	Set Channel ID
		buffer[3] =id[0];						//DATA1 Channel Number 
		buffer[4] =id[1];						//DATA2 Device Number (ER propritary)
		buffer[5] =id[2];						//DATA2 Device Number (ER propritary) 16bits 0 - 65526
		buffer[6] =id[3];						//DATA3 Device Type
		buffer[7] =id[4];						//DATA4 Device Transmission
			
		SEND_ANT(buffer);
}

void ANT_OpenChannel(int channel[])
{int buffer[4];
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x01;						//LENGTH
		buffer[2] =0x4B;						//MESSAGE ID 	Open Channel
		buffer[3] =channel[0];					//DATA1 Channel Number
		
		SEND_ANT(buffer);
		
}

void ANT_CloseChannel(int channel[])
{int buffer[4];
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x01;						//LENGTH
		buffer[2] =0x4C;						//MESSAGE ID 	Open Channel
		buffer[3] =channel[0];					//DATA1 Channel Number
		
		SEND_ANT(buffer);
		
}

void ANT_SendBroadcastData(int channel[], int *ptr)
{
union PackByte ANTData;

		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x09;						//LENGTH
		buffer[2] =0x4E;						//MESSAGE ID 	Send data
		buffer[3]  =channel[0];					//channel #
		ANTData.word = *(ptr + roffset);
		buffer[4]  =ANTData.HighByte;			//DATA0
		buffer[5]  =ANTData.LowByte;			//DATA1
		ANTData.word = *(ptr + xoffset);
		buffer[6]  =ANTData.HighByte;			//DATA2
		buffer[7]  =ANTData.LowByte;			//DATA3
		ANTData.word = *(ptr + yoffset);
		buffer[8]  =ANTData.HighByte;			//DATA4
		buffer[9]  =ANTData.LowByte;			//DATA5
		ANTData.word = *(ptr + zoffset);
		buffer[10] =ANTData.HighByte;			//DATA6
		buffer[11] =ANTData.LowByte;			//DATA7	
		
		SEND_ANT(buffer);
}

void ANT_SendAcknowledgeData(int channel[], int *ptr)
{
union PackByte ANTData;
		
		buffer[0] =0xA4;						//SYNC
		buffer[1] =0x09;						//LENGTH
		buffer[2] =0x4F;						//MESSAGE ID 	Send data
		buffer[3]  =channel[0];					//channel #
		ANTData.word = *(ptr + roffset);
		buffer[4]  =ANTData.HighByte;			//DATA0
		buffer[5]  =ANTData.LowByte;			//DATA1
		ANTData.word = *(ptr + xoffset);
		buffer[6]  =ANTData.HighByte;			//DATA2
		buffer[7]  =ANTData.LowByte;			//DATA3
		ANTData.word = *(ptr + yoffset);
		buffer[8]  =ANTData.HighByte;			//DATA4
		buffer[9]  =ANTData.LowByte;			//DATA5
		ANTData.word = *(ptr + zoffset);
		buffer[10] =ANTData.HighByte;			//DATA6
		buffer[11] =ANTData.LowByte;			//DATA7		
		
		SEND_ANT(buffer);
}

void ANT_send_burst_data(int channel[], int *ptr)
{
union PackByte ANTData;

		buffer[0] 	=0xA4;						//SYNC
		buffer[1] 	=0x09;						//LENGTH
		buffer[2] 	=0x50;						//MESSAGE ID 	Send data
		buffer[3] 	=channel[0];				//DATA1
		ANTData.word = *(ptr + roffset);
		buffer[4]  =ANTData.HighByte;			//DATA0
		buffer[5]  =ANTData.LowByte;			//DATA1
		ANTData.word = *(ptr + xoffset);
		buffer[6]  =ANTData.HighByte;			//DATA2
		buffer[7]  =ANTData.LowByte;			//DATA3
		ANTData.word = *(ptr + yoffset);
		buffer[8]  =ANTData.HighByte;			//DATA4
		buffer[9]  =ANTData.LowByte;			//DATA5
		ANTData.word = *(ptr + zoffset);
		buffer[10] =ANTData.HighByte;			//DATA6
		buffer[11] =ANTData.LowByte;			//DATA7	
		
		SEND_ANT(buffer);
}
