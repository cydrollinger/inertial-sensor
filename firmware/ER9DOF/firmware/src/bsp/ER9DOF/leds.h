/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *******************************************************************/

#ifndef LEDS_H
#define LEDS_H

#include <stdbool.h>

#define             blue_pwm            0x12                    //function number for output compare OC1
#define             green_pwm           0x13                    //function number for output compare OC2
#define             red_pwm             0x14                    //function number for output compare OC3 pg 127 DS

/* PWM COLOR LEDS */
#define             zero                0x00
#define             ten_percent         0x7CF                   //Ouput Capture duty cycle
#define             one_percent         0xCF                    //Ouput Capture period based on timer1 prescaler 8
#define             less_than_apercent  0x4E0B                  //a tenth of a percent
#define             one_hundred_percent 0x4E1F
#define             one_hundred_Hz      0x4E10
#define             pwm                 0x06
#define             tim_1               0x04
#define             sync_source         0x1F


void INITIALIZE_LEDs(void);
void BLUE_on(int);
int  is_BLUE_on(void);
void GREEN_on(int);
int  is_GREEN_on(void);
void RED_on(int);
void RED_off(void);
int  is_RED_on(void);
void PURPLE_on(int);
void AQUA_on(int);
void ORANGE_on(int);
void LEDs_on(int);
void TOGGLE_LED(void);
void LED_off(void);
void WHITE_on(int);

#endif //LEDS_H
