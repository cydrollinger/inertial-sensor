/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *******************************************************************/

#include <xc.h>
#include <leds.h>
#include <stdbool.h>

#define LED_ON  0
#define LED_OFF 1

#define INPUT  1
#define OUTPUT 0

int prev_OC1R,prev_OC2R,prev_OC3R;

//union TwoWords timer32bits;

void INITIALIZE_LEDs(void)
{

_TRISB13 = OUTPUT;
_TRISB14 = OUTPUT;
_TRISB15 = OUTPUT;

_RP13R = red_pwm;							
_RP15R = green_pwm;
_RP14R = blue_pwm;							

T1CON = 0x8010;                                         //Timer 1 runs the PWM for the LED brightness

/* BLUE*/
OC1R = one_hundred_percent;                             //duty cycle (0% - 0x000)
OC1RS = one_hundred_Hz;                                 //period based on 16MHz clock and pre scaler = 8
OC1CON2bits.SYNCSEL = sync_source;                      //source is the output capture itself
OC1CON2bits.OCTRIG = zero;
OC1CON1bits.OCTSEL = tim_1;
OC1CON1bits.OCM = pwm;                                  //OCM enables the pwm and disables

/*GREEN*/
OC2R = one_hundred_percent;                             //duty cycle (0% - 0x0000)
OC2RS = one_hundred_Hz;                                 //period based on 16MHz clock and prescaler = 8
OC2CON2bits.SYNCSEL = sync_source;                      //source is the output capture itself
OC2CON2bits.OCTRIG = zero;
OC2CON1bits.OCTSEL = tim_1;
OC2CON1bits.OCM = pwm;                                  //OCM enables the pwm and disables

/*RED*/
OC3R = one_hundred_percent;									
OC3RS = one_hundred_Hz;                                 //period based on 16MHz clock and pre scaler = 8
OC3CON2bits.SYNCSEL = sync_source;                      //source is the output capture itself
OC3CON2bits.OCTRIG = zero;
OC3CON1bits.OCTSEL = tim_1;
OC3CON1bits.OCM = pwm;                                  //OCM enables the pwm and disables

PR1 = one_hundred_Hz;                                   //T2 timer

BLUE_on(less_than_apercent);
//_RB13 = 1;                                            //Device #3 has a bad output on red led 
}

void LED_off()
{
OC1R =	one_hundred_percent;
OC2R =	one_hundred_percent;                            //
OC3R =	one_hundred_percent;                            //duty cycle (10% - 7CF)     (1% - CF)
}

void BLUE_on(int duty)
{
OC1R =	duty;
OC2R =	one_hundred_percent;                            //
OC3R =	one_hundred_percent;                            //duty cycle (10% - 7CF)     (1% - CF)
}

int is_BLUE_on (void)
{
return(OC1R - one_hundred_percent);
}


void GREEN_on(int duty)
{
OC1R =	one_hundred_percent;
OC2R =	duty;
OC3R =	one_hundred_percent;
}

int is_GREEN_on (void)
{
return(OC2R - one_hundred_percent);
}

void RED_on(int duty)
{
OC1R =	one_hundred_percent;
OC2R =	one_hundred_percent;
OC3R =	duty;                                           //duty cycle (10% - 7CF)     (1% - CF) 
}

void RED_off(void)
{
OC3R =	one_hundred_percent;                                           //duty cycle (10% - 7CF)     (1% - CF) 
}

int is_RED_on (void)
{
return(OC3R - one_hundred_percent);
}

void ORANGE_on(int duty)
{
OC1R = one_hundred_percent;
OC2R = duty;
OC3R = duty;
}

int is_ORANGE_on (void)
{
return((OC3R - one_hundred_percent) & (OC2R - one_hundred_percent));
}

void PURPLE_on(int duty)
{
OC1R = duty;
OC2R = one_hundred_percent;
OC3R = duty;
}
int is_PURPLE_on (void)
{
return((OC1R - one_hundred_percent) & (OC3R - one_hundred_percent));
}

void AQUA_on(int duty)
{
OC1R = duty;
OC2R = duty;
OC3R = one_hundred_percent;
}
int is_AQUA_on (void)
{
return((OC1R - one_hundred_percent) & (OC2R - one_hundred_percent));
}

void WHITE_on(int duty)
{
OC1R = duty;
OC2R = duty;
OC3R = duty;
}

int is_WHITE_on (void)
{
return((OC1R - one_hundred_percent) & (OC2R - one_hundred_percent) & (OC3R - one_hundred_percent));
}

int is_LED_on (void)
{
return((OC1R - one_hundred_percent) | (OC2R - one_hundred_percent) | (OC3R - one_hundred_percent));
}

void Toggle_LED(void)
{
    if(is_LED_on())
    {
        prev_OC1R = OC1R;
        prev_OC2R = OC2R;
        prev_OC3R = OC3R;
        LED_off();
    }
    else
    {
        OC1R = prev_OC1R;
        OC2R = prev_OC2R;
        OC3R = prev_OC3R;
    }

}

long int getTick(void)
{
    return(TMR2);
}