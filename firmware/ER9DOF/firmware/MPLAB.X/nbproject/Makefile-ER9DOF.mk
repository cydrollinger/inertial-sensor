#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-ER9DOF.mk)" "nbproject/Makefile-local-ER9DOF.mk"
include nbproject/Makefile-local-ER9DOF.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=ER9DOF
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/app/system_config/ER9DOF/system.c ../src/main.c ../src/app/NineDOF.c ../src/app/interrupt.c ../src/app/Mbed_itoa.s ../src/app/app_device_msd.c ../src/app/usb_descriptors.c ../src/bsp/ER9DOF/buttons.c ../src/bsp/ER9DOF/leds.c ../src/bsp/ER9DOF/rtcc.c ../src/framework/driver/spi/drv_spi_16bit.c ../src/framework/fileio/fileio.c ../src/framework/fileio/sd_spi.c ../src/framework/usb/usb_device.c ../src/framework/usb/usb_device_msd_multi_sector.c ../src/framework/usb/usb_hal_pic24.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/567897268/system.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/659850791/NineDOF.o ${OBJECTDIR}/_ext/659850791/interrupt.o ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o ${OBJECTDIR}/_ext/659850791/app_device_msd.o ${OBJECTDIR}/_ext/659850791/usb_descriptors.o ${OBJECTDIR}/_ext/1152322425/buttons.o ${OBJECTDIR}/_ext/1152322425/leds.o ${OBJECTDIR}/_ext/1152322425/rtcc.o ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o ${OBJECTDIR}/_ext/1442584205/fileio.o ${OBJECTDIR}/_ext/1442584205/sd_spi.o ${OBJECTDIR}/_ext/175103367/usb_device.o ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/567897268/system.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/659850791/NineDOF.o.d ${OBJECTDIR}/_ext/659850791/interrupt.o.d ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d ${OBJECTDIR}/_ext/659850791/app_device_msd.o.d ${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d ${OBJECTDIR}/_ext/1152322425/buttons.o.d ${OBJECTDIR}/_ext/1152322425/leds.o.d ${OBJECTDIR}/_ext/1152322425/rtcc.o.d ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d ${OBJECTDIR}/_ext/1442584205/fileio.o.d ${OBJECTDIR}/_ext/1442584205/sd_spi.o.d ${OBJECTDIR}/_ext/175103367/usb_device.o.d ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/567897268/system.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/659850791/NineDOF.o ${OBJECTDIR}/_ext/659850791/interrupt.o ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o ${OBJECTDIR}/_ext/659850791/app_device_msd.o ${OBJECTDIR}/_ext/659850791/usb_descriptors.o ${OBJECTDIR}/_ext/1152322425/buttons.o ${OBJECTDIR}/_ext/1152322425/leds.o ${OBJECTDIR}/_ext/1152322425/rtcc.o ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o ${OBJECTDIR}/_ext/1442584205/fileio.o ${OBJECTDIR}/_ext/1442584205/sd_spi.o ${OBJECTDIR}/_ext/175103367/usb_device.o ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o

# Source Files
SOURCEFILES=../src/app/system_config/ER9DOF/system.c ../src/main.c ../src/app/NineDOF.c ../src/app/interrupt.c ../src/app/Mbed_itoa.s ../src/app/app_device_msd.c ../src/app/usb_descriptors.c ../src/bsp/ER9DOF/buttons.c ../src/bsp/ER9DOF/leds.c ../src/bsp/ER9DOF/rtcc.c ../src/framework/driver/spi/drv_spi_16bit.c ../src/framework/fileio/fileio.c ../src/framework/fileio/sd_spi.c ../src/framework/usb/usb_device.c ../src/framework/usb/usb_device_msd_multi_sector.c ../src/framework/usb/usb_hal_pic24.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-ER9DOF.mk dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FJ64GB004
MP_LINKER_FILE_OPTION=,--script=../src/linker/app_hid_boot_p24FJ64GB004.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/567897268/system.o: ../src/app/system_config/ER9DOF/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/567897268" 
	@${RM} ${OBJECTDIR}/_ext/567897268/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/567897268/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/system_config/ER9DOF/system.c  -o ${OBJECTDIR}/_ext/567897268/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/567897268/system.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/567897268/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/main.c  -o ${OBJECTDIR}/_ext/1360937237/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/NineDOF.o: ../src/app/NineDOF.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/NineDOF.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/NineDOF.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/NineDOF.c  -o ${OBJECTDIR}/_ext/659850791/NineDOF.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/NineDOF.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/NineDOF.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/interrupt.o: ../src/app/interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/interrupt.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/interrupt.c  -o ${OBJECTDIR}/_ext/659850791/interrupt.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/interrupt.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/interrupt.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/app_device_msd.o: ../src/app/app_device_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/app_device_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/app_device_msd.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/app_device_msd.c  -o ${OBJECTDIR}/_ext/659850791/app_device_msd.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/app_device_msd.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/app_device_msd.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/usb_descriptors.o: ../src/app/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/usb_descriptors.c  -o ${OBJECTDIR}/_ext/659850791/usb_descriptors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/buttons.o: ../src/bsp/ER9DOF/buttons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/buttons.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/buttons.c  -o ${OBJECTDIR}/_ext/1152322425/buttons.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/buttons.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/buttons.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/leds.o: ../src/bsp/ER9DOF/leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/leds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/leds.c  -o ${OBJECTDIR}/_ext/1152322425/leds.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/leds.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/leds.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/rtcc.o: ../src/bsp/ER9DOF/rtcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/rtcc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/rtcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/rtcc.c  -o ${OBJECTDIR}/_ext/1152322425/rtcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/rtcc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/rtcc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o: ../src/framework/driver/spi/drv_spi_16bit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/928156496" 
	@${RM} ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d 
	@${RM} ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/driver/spi/drv_spi_16bit.c  -o ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1442584205/fileio.o: ../src/framework/fileio/fileio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1442584205" 
	@${RM} ${OBJECTDIR}/_ext/1442584205/fileio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1442584205/fileio.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/fileio/fileio.c  -o ${OBJECTDIR}/_ext/1442584205/fileio.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1442584205/fileio.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1442584205/fileio.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1442584205/sd_spi.o: ../src/framework/fileio/sd_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1442584205" 
	@${RM} ${OBJECTDIR}/_ext/1442584205/sd_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1442584205/sd_spi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/fileio/sd_spi.c  -o ${OBJECTDIR}/_ext/1442584205/sd_spi.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1442584205/sd_spi.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1442584205/sd_spi.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_device.o: ../src/framework/usb/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_device.c  -o ${OBJECTDIR}/_ext/175103367/usb_device.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_device.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_device.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o: ../src/framework/usb/usb_device_msd_multi_sector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_device_msd_multi_sector.c  -o ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o: ../src/framework/usb/usb_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_hal_pic24.c  -o ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/567897268/system.o: ../src/app/system_config/ER9DOF/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/567897268" 
	@${RM} ${OBJECTDIR}/_ext/567897268/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/567897268/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/system_config/ER9DOF/system.c  -o ${OBJECTDIR}/_ext/567897268/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/567897268/system.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/567897268/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/main.c  -o ${OBJECTDIR}/_ext/1360937237/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/NineDOF.o: ../src/app/NineDOF.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/NineDOF.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/NineDOF.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/NineDOF.c  -o ${OBJECTDIR}/_ext/659850791/NineDOF.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/NineDOF.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/NineDOF.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/interrupt.o: ../src/app/interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/interrupt.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/interrupt.c  -o ${OBJECTDIR}/_ext/659850791/interrupt.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/interrupt.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/interrupt.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/app_device_msd.o: ../src/app/app_device_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/app_device_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/app_device_msd.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/app_device_msd.c  -o ${OBJECTDIR}/_ext/659850791/app_device_msd.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/app_device_msd.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/app_device_msd.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/659850791/usb_descriptors.o: ../src/app/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app/usb_descriptors.c  -o ${OBJECTDIR}/_ext/659850791/usb_descriptors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/usb_descriptors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/buttons.o: ../src/bsp/ER9DOF/buttons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/buttons.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/buttons.c  -o ${OBJECTDIR}/_ext/1152322425/buttons.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/buttons.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/buttons.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/leds.o: ../src/bsp/ER9DOF/leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/leds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/leds.c  -o ${OBJECTDIR}/_ext/1152322425/leds.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/leds.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/leds.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1152322425/rtcc.o: ../src/bsp/ER9DOF/rtcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1152322425" 
	@${RM} ${OBJECTDIR}/_ext/1152322425/rtcc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1152322425/rtcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/bsp/ER9DOF/rtcc.c  -o ${OBJECTDIR}/_ext/1152322425/rtcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1152322425/rtcc.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1152322425/rtcc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o: ../src/framework/driver/spi/drv_spi_16bit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/928156496" 
	@${RM} ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d 
	@${RM} ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/driver/spi/drv_spi_16bit.c  -o ${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/928156496/drv_spi_16bit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1442584205/fileio.o: ../src/framework/fileio/fileio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1442584205" 
	@${RM} ${OBJECTDIR}/_ext/1442584205/fileio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1442584205/fileio.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/fileio/fileio.c  -o ${OBJECTDIR}/_ext/1442584205/fileio.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1442584205/fileio.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1442584205/fileio.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1442584205/sd_spi.o: ../src/framework/fileio/sd_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1442584205" 
	@${RM} ${OBJECTDIR}/_ext/1442584205/sd_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1442584205/sd_spi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/fileio/sd_spi.c  -o ${OBJECTDIR}/_ext/1442584205/sd_spi.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1442584205/sd_spi.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1442584205/sd_spi.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_device.o: ../src/framework/usb/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_device.c  -o ${OBJECTDIR}/_ext/175103367/usb_device.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_device.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_device.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o: ../src/framework/usb/usb_device_msd_multi_sector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_device_msd_multi_sector.c  -o ${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_device_msd_multi_sector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o: ../src/framework/usb/usb_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/175103367" 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/framework/usb/usb_hal_pic24.c  -o ${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d"        -g -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/_ext/175103367/usb_hal_pic24.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/659850791/Mbed_itoa.o: ../src/app/Mbed_itoa.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../src/app/Mbed_itoa.s  -o ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -Wa,-MD,"${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/_ext/659850791/Mbed_itoa.o: ../src/app/Mbed_itoa.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659850791" 
	@${RM} ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d 
	@${RM} ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../src/app/Mbed_itoa.s  -o ${OBJECTDIR}/_ext/659850791/Mbed_itoa.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -Wa,-MD,"${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/_ext/659850791/Mbed_itoa.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/linker/app_hid_boot_p24FJ64GB004.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF"  -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/linker/app_hid_boot_p24FJ64GB004.gld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -no-legacy-libc  -I"../src/app/system_config/ER9DOF" -I"../src/app" -I"../src/framework/usb" -I"../src/framework/driver/spi" -I"../src/framework/fileio" -I"../src/bsp/ER9DOF" -Wl,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/ER9DOF
	${RM} -r dist/ER9DOF

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
