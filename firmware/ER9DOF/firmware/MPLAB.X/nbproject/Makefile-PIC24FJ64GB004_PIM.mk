#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-PIC24FJ64GB004_PIM.mk)" "nbproject/Makefile-local-PIC24FJ64GB004_PIM.mk"
include nbproject/Makefile-local-PIC24FJ64GB004_PIM.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=PIC24FJ64GB004_PIM
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/system_config/exp16/pic24fj64gb004_pim/system.c ../src/main.c ../src/usb_descriptors.c ../src/app_device_msd.c ../../../../../../framework/driver/spi/src/drv_spi_16bit.c ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c ../../../../../../framework/usb/src/usb_device.c ../../../../../../framework/usb/src/usb_hal_pic24.c ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c ../src/system_config/ER9DOF/system.c ../../../../../../bsp/ER9DOF/buttons.c ../../../../../../bsp/ER9DOF/leds.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/621193660/system.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o ${OBJECTDIR}/_ext/1360937237/app_device_msd.o ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o ${OBJECTDIR}/_ext/1226487842/sd_spi.o ${OBJECTDIR}/_ext/838585624/usb_device.o ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o ${OBJECTDIR}/_ext/1503359622/system.o ${OBJECTDIR}/_ext/405039903/buttons.o ${OBJECTDIR}/_ext/405039903/leds.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/621193660/system.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d ${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d ${OBJECTDIR}/_ext/1226487842/sd_spi.o.d ${OBJECTDIR}/_ext/838585624/usb_device.o.d ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d ${OBJECTDIR}/_ext/1503359622/system.o.d ${OBJECTDIR}/_ext/405039903/buttons.o.d ${OBJECTDIR}/_ext/405039903/leds.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/621193660/system.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o ${OBJECTDIR}/_ext/1360937237/app_device_msd.o ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o ${OBJECTDIR}/_ext/1226487842/sd_spi.o ${OBJECTDIR}/_ext/838585624/usb_device.o ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o ${OBJECTDIR}/_ext/1503359622/system.o ${OBJECTDIR}/_ext/405039903/buttons.o ${OBJECTDIR}/_ext/405039903/leds.o

# Source Files
SOURCEFILES=../src/system_config/exp16/pic24fj64gb004_pim/system.c ../src/main.c ../src/usb_descriptors.c ../src/app_device_msd.c ../../../../../../framework/driver/spi/src/drv_spi_16bit.c ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c ../../../../../../framework/usb/src/usb_device.c ../../../../../../framework/usb/src/usb_hal_pic24.c ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c ../src/system_config/ER9DOF/system.c ../../../../../../bsp/ER9DOF/buttons.c ../../../../../../bsp/ER9DOF/leds.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-PIC24FJ64GB004_PIM.mk dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FJ64GB004
MP_LINKER_FILE_OPTION=,--script=p24FJ64GB004.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/621193660/system.o: ../src/system_config/exp16/pic24fj64gb004_pim/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/621193660" 
	@${RM} ${OBJECTDIR}/_ext/621193660/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/621193660/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/pic24fj64gb004_pim/system.c  -o ${OBJECTDIR}/_ext/621193660/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/621193660/system.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/621193660/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/main.c  -o ${OBJECTDIR}/_ext/1360937237/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/usb_descriptors.o: ../src/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/usb_descriptors.c  -o ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/app_device_msd.o: ../src/app_device_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_device_msd.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app_device_msd.c  -o ${OBJECTDIR}/_ext/1360937237/app_device_msd.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o: ../../../../../../framework/driver/spi/src/drv_spi_16bit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/368788181" 
	@${RM} ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d 
	@${RM} ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/driver/spi/src/drv_spi_16bit.c  -o ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1226487842/sd_spi.o: ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1226487842" 
	@${RM} ${OBJECTDIR}/_ext/1226487842/sd_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1226487842/sd_spi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c  -o ${OBJECTDIR}/_ext/1226487842/sd_spi.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1226487842/sd_spi.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1226487842/sd_spi.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_device.o: ../../../../../../framework/usb/src/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_device.c  -o ${OBJECTDIR}/_ext/838585624/usb_device.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_device.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_device.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o: ../../../../../../framework/usb/src/usb_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_hal_pic24.c  -o ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o: ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c  -o ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1503359622/system.o: ../src/system_config/ER9DOF/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1503359622" 
	@${RM} ${OBJECTDIR}/_ext/1503359622/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/1503359622/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/ER9DOF/system.c  -o ${OBJECTDIR}/_ext/1503359622/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1503359622/system.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1503359622/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/405039903/buttons.o: ../../../../../../bsp/ER9DOF/buttons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/405039903" 
	@${RM} ${OBJECTDIR}/_ext/405039903/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/405039903/buttons.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../bsp/ER9DOF/buttons.c  -o ${OBJECTDIR}/_ext/405039903/buttons.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/405039903/buttons.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/405039903/buttons.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/405039903/leds.o: ../../../../../../bsp/ER9DOF/leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/405039903" 
	@${RM} ${OBJECTDIR}/_ext/405039903/leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/405039903/leds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../bsp/ER9DOF/leds.c  -o ${OBJECTDIR}/_ext/405039903/leds.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/405039903/leds.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/405039903/leds.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/621193660/system.o: ../src/system_config/exp16/pic24fj64gb004_pim/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/621193660" 
	@${RM} ${OBJECTDIR}/_ext/621193660/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/621193660/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/exp16/pic24fj64gb004_pim/system.c  -o ${OBJECTDIR}/_ext/621193660/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/621193660/system.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/621193660/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/main.c  -o ${OBJECTDIR}/_ext/1360937237/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/usb_descriptors.o: ../src/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/usb_descriptors.c  -o ${OBJECTDIR}/_ext/1360937237/usb_descriptors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/usb_descriptors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/app_device_msd.o: ../src/app_device_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_device_msd.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/app_device_msd.c  -o ${OBJECTDIR}/_ext/1360937237/app_device_msd.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app_device_msd.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o: ../../../../../../framework/driver/spi/src/drv_spi_16bit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/368788181" 
	@${RM} ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d 
	@${RM} ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/driver/spi/src/drv_spi_16bit.c  -o ${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/368788181/drv_spi_16bit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1226487842/sd_spi.o: ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1226487842" 
	@${RM} ${OBJECTDIR}/_ext/1226487842/sd_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/1226487842/sd_spi.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/fileio/drivers/sd_spi/sd_spi.c  -o ${OBJECTDIR}/_ext/1226487842/sd_spi.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1226487842/sd_spi.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1226487842/sd_spi.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_device.o: ../../../../../../framework/usb/src/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_device.c  -o ${OBJECTDIR}/_ext/838585624/usb_device.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_device.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_device.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o: ../../../../../../framework/usb/src/usb_hal_pic24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_hal_pic24.c  -o ${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_hal_pic24.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o: ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/838585624" 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d 
	@${RM} ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../framework/usb/src/usb_device_msd_multi_sector.c  -o ${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/838585624/usb_device_msd_multi_sector.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1503359622/system.o: ../src/system_config/ER9DOF/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1503359622" 
	@${RM} ${OBJECTDIR}/_ext/1503359622/system.o.d 
	@${RM} ${OBJECTDIR}/_ext/1503359622/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../src/system_config/ER9DOF/system.c  -o ${OBJECTDIR}/_ext/1503359622/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1503359622/system.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/1503359622/system.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/405039903/buttons.o: ../../../../../../bsp/ER9DOF/buttons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/405039903" 
	@${RM} ${OBJECTDIR}/_ext/405039903/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/405039903/buttons.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../bsp/ER9DOF/buttons.c  -o ${OBJECTDIR}/_ext/405039903/buttons.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/405039903/buttons.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/405039903/buttons.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/405039903/leds.o: ../../../../../../bsp/ER9DOF/leds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/405039903" 
	@${RM} ${OBJECTDIR}/_ext/405039903/leds.o.d 
	@${RM} ${OBJECTDIR}/_ext/405039903/leds.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../../../../../../bsp/ER9DOF/leds.c  -o ${OBJECTDIR}/_ext/405039903/leds.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/405039903/leds.o.d"        -g -omf=elf -fno-short-double -O0 -falign-arrays -I"../src" -I"../../../../../../framework/usb/inc" -I"../../../../../../framework/fileio/inc" -I"../../../../../../framework" -I"../../../../../../bsp/exp16/pic24fj64gb004_pim" -I"../src/system_config/exp16/pic24fj64gb004_pim" -I"../../../../../../framework/fileio/drivers/sd_spi" -I"../../../../../../framework/driver/spi" -msmart-io=1 -msfr-warn=off -fno-ivopts
	@${FIXDEPS} "${OBJECTDIR}/_ext/405039903/leds.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf  -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../../../bootloaders/firmware/pic24_dspic/MPLAB.X/dist/ER9DOF/production/MPLAB.X.production.hex
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -Wl,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
	@echo "Creating unified hex file"
	@"C:/Program Files/Microchip/MPLABX/mplab_ide/mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files/Microchip/MPLABX/mplab_ide/mplab_ide/modules/../../dat/en_msgs.txt" -break=157F0  dist/${CND_CONF}/${IMAGE_TYPE}/MPLAB.X.${IMAGE_TYPE}.hex ../../../bootloaders/firmware/pic24_dspic/MPLAB.X/dist/ER9DOF/production/MPLAB.X.production.hex -odist/${CND_CONF}/production/MPLAB.X.production.unified.hex

endif


# Subprojects
.build-subprojects:
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
	cd /D ../../../bootloaders/firmware/pic24_dspic/MPLAB.X && ${MAKE}  -f Makefile CONF=ER9DOF TYPE_IMAGE=DEBUG_RUN
else
	cd /D ../../../bootloaders/firmware/pic24_dspic/MPLAB.X && ${MAKE}  -f Makefile CONF=ER9DOF
endif


# Subprojects
.clean-subprojects:
	cd /D ../../../bootloaders/firmware/pic24_dspic/MPLAB.X && rm -rf "build/ER9DOF" "dist/ER9DOF"

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/PIC24FJ64GB004_PIM
	${RM} -r dist/PIC24FJ64GB004_PIM

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
