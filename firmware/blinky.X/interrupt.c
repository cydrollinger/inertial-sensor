//***********************************************************************//
//********************   Interrupts  ************************************//
//***********************************************************************//

#include "interrupt.h"
#include "er_it_io.h"

void INITIALIZE_interrupts (void)
{
INTCON1 = 0x0000;							//setting nested interrupts bit 15 = 0

T2CON = 0x8030;								//32 bit timer with a one to one clock with the system clock
PR2 = 0xF424;
_T2IF = off;
_T2IE = on;

}

void __attribute__((interrupt, no_auto_psv))  _T2Interrupt (void)
{
    Toggle_LED();
    _T2IF = off;
}

