/********************************************
Company: Electronic Realization L.L.C.
Address: 313 W. Mendenhall #5
			Bozeman, MT 59715
Phone: 406-586-5502
Email: cy@montana.net
Author: Cy Drollinger
Date: 3/2/2010

*********
HARDWARE
*********
Microchip MCU: PIC24FJ64B004
	PIN 12-15:	LY503ALH	 
GPIO
	RA10		HP (High pass filter)
	RA7			ST	(Self Test)
ADC	
	RB14/AN10	OUT-
	RB15/AN9	OUT+
	
	PIN 23-27 	ADXL345
SPI	
	RP2			SCK_ACCL	max 5Mhz
	RP3			SDI_ACCL
	RP16		SDO_ACCL
GPIO
	RP17		INT_2_ACCL
	RP18		IN_ACCL
	
	PIN 2 - 5	M25P16 -128
SPI	
	RC6			SCK_MEM		max 20 - 50 Mhz
	RC7			SDI_MEM
	RC8			SDO_MEM
	RC9			SS_MEM
	
	PIN 36 - 38, 41, 43, 44
				nRF24AP2
GPIO	
	RP16		RESET
	RP20		RTS
	RP21		SLEEP
	RB5			SUSP
UART	
	RP7			UART_RX
	RP8			UART_TX
	
LEDS
	GREEN anode35 cathode34
	RED	  anode34 cathode35		
	
ST MicroElectonics Gyroscope: LY503ALH
Analog Devices Accelerometer: ADXL345
ST MicroElectonics SPE Flash: M25P16 -128
Nordic Semiconductor ANT+ : nRF24AP2
Green Led
Red Led

*************
Description
*************
HP_custom - a datalogger for three datum 
	two accerlations from ADXL345 (SPI)
	one rotation from LY503ALH (diff. ADC)
	Store into SPI Flash M25P16-128 (SPI)
	upload dat through USB 
	Uplaod data over ant+ nRFAP2 (UART +) 
*******************************************/
#ifndef ER_IT_IO_H 
#define ER_IT_IO_H

#include "Compiler.h"

union TwoWords
{
 long int two_words;
 struct
 	{
    int firstWord;				//lsb
    int secondWord;				//msb
  	};
};

/** Alleviate magic numbers ******************************************/
		/** General**/
#define INPUT_PIN   1
#define OUTPUT_PIN  0
#define on          1
#define off         0
#define zero        0
#define one         1
#define one_second  0xF424
		

#define	blue        _LATB14				//pin 20 RP6
#define green       _LATB15				//pin 21 RP0
#define red         _LATB13				//pin 22 RP1

#define             blue_pwm            0x12		//function number for output compare OC1
#define             green_pwm           0x13		//function number for output compare OC2
#define             red_pwm             0x14		//function number for output compare OC3 pg 127 DS

/* PWM COLOR LEDS */
#define             ten_percent 0x7CF                   //Ouput Capture duty cycle
#define             one_percent 0xCF                    //Ouput Capture period based on timer1 prescaler 8
#define             less_then_apercent 0x4E0B           //a tenth of a percent
#define             one_hundred_percent 0x4E1F
#define             one_hundred_Hz 0x4E10
#define             pwm 		0x06
#define             tim_1		0x04
#define             sync_source	0x1F


void INITIALIZE_LEDs(void);
void BLUE_on(int);
int  is_BLUE_on(void);
void GREEN_on(int);
int  is_GREEN_on(void);
void RED_on(int);
int  is_RED_on(void);
void PURPLE_on(int);
void AQUA_on(int);
void ORANGE_on(int);
void LEDs_on(int);
#endif /* ER_IT_IO_H */
