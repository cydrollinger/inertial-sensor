**************************************************************************
* Electronic Realization L.L.C.						 *
* Design: 	9 navigational sensors Accelerometer-XYZ, Gyroscopes-XYZ,* 
*			Magnetometer-XYZ				 *
*						ER 9DOF			 *
* Engineer: Cy Drollinger						 *
* Date: December 15th, 2015						 *	
* Email: cy@elec-real.com						 *
* Address: Box 191 Bozeman, MT 59771-0191				 *
* Phone: 406-539-8117							 *
**************************************************************************

PROGRAMMING THE ER9DOF

These files within the programming folder are provided by Microchip inc. 
and a freely distributed in order to make their IC the PIC24FJ64GB004 
programmable via the HIDBootloader.exe program and a USB port.

1. Copy this folder onto the desktop of a windows PC. WARNING: do not 
attempt to program the ER9DOF from it's own drive.
2. double click the executable HIDBootloader.exe.
3. Plug in the ER9DOF into a USB port on the same machine.
	a. ER9DOF must be off, switch toward the center
	b. ER9DOF's top button must be pressed while plugging into usb.
4. HIDBootloader.exe should have recognized thtat the device is attached.
5. Open a hex file to program the ER9DOF.
	a. The hex file is the machine language to reprogram the ER9DOF
	   with updates and new features.  
 