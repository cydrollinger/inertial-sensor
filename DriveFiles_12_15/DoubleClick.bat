@ECHO off
@SET /P file=Enter the file name of data to be plotted:
copy %file% graph\data
cd graph\data
rename %file% data.csv
parse
cd ..
"NineDegreesGraphed.html"
pause
cd data
del accelerate.txt
del gyrate.txt
del magnetic.txt
del temp.txt
del data.csv 
del data.js


