**************************************************************************
* Electronic Realization L.L.C.						 *
* Design: 	9 navigational sensors Accelerometer-XYZ, Gyroscopes-XYZ,* 
*			Magnetometer-XYZ				 *
*						ER 9DOF			 *
* Engineer: Cy Drollinger						 *
* Date: December 15th, 2015						 *	
* Email: cy@elec-real.com						 *
* Address: Box 191 Bozeman, MT 59771-0191				 *
* Phone: 406-539-8117							 *
**************************************************************************

*******************************************
*File Arcitecture plus a terse description*
*******************************************

ROOT:
	Graph:	
		Folder containing the neccessary files to display data within a 
		web-browser graph. This is made possible through Dygraphs: a 
		flexible open source JavaScript charting library.
		
		dygraph-combined.js: 
			Dygraphs open source Javascript charting library
		
		NineDegreesGraphed.html: 
			An Edited HTML file to display ERDOF data.
		
		readme.txt:
		
		data: 
			Folder to contain the executable to parse ER9DOF to the
			format for Dygraph and NineDegreesGraphed.html and the transient
			data: 
				dataxxxx.csv, accelerometer.txt,gyrate.txt,magnetometer.txt,
				and temp.txt.
			
			parse: 
				the executable to transform raw data into seperate files

			src: 
				Folder containing the source code for parse.txt and a borland
				c compiler
				
				parsce.cpp: file
				Ccompiler: compiler
 
	
	Programming: 
		Folder containing the .dll files neccessary to reprogram ER9DOF
		via the executable, HIDBootloader.exe, and a usb port.
			
			WARNING: Do not attempt to program your ER9DOF from the drive itself.
			The file need to be copied to a PC to program the ER9DOF.
		Hex: 
			Folder containing the current hex files to program the ER9DOF device.
			They may be updates and new features.
	
	Autorun.inf: 
		A windows based text file to automate the opening of the root.
	
	DoubleClick.bat: 
		A batch file utilizing dos commands. First, the batch prompts
		user for the name of the file to copy data into the	Graph/data folder to be
		parsed into four files. The NineDegreesgraphed.html	file is opened with the
		default web browser displaying the four data files. And finally	prompting for
		a key press to clean the data folder to begin again.
	
	ER.ico: a graphic utilized by Windows OS that displays within expoler. 
	
	readme.txt: This file
		